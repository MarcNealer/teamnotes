__author__ = 'marc'
from rest_framework.views import APIView
from rest_framework.response import Response
from permissions import IsAppAdmin
from models import Account
from serilizers import TierSerializer
from rest_framework import status
from django.http import Http404

class TierDetail(APIView):
    """
    Retrieve Tier detauls
    """
    def get_object(self, request):
        try:
            return Account.objects.get(users=request.user).tier
        except Account.DoesNotExist:
            raise Http404

    def get(self, request, format=None):
        tier = self.get_object(request)
        serializer = TierSerializer(tier)
        return Response(serializer.data)