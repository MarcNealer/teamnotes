__author__ = 'marc'
from rest_framework.views import APIView
from rest_framework.response import Response
from permissions import IsAppAdmin
from models import Account
from models import Notes
from serilizers import AccountRecordSerializer
from rest_framework import status
from django.http import Http404
from AccountCheck import *

class AccountDetail(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    permission_classes = (IsAppAdmin,)
    def get_object(self, request, pk):
        try:
            return Account.objects.get(pk=pk)
        except Account.DoesNotExist:
            raise Http404



    def get(self, request, pk, format=None):
        account = self.get_object(request, pk)
        serializer = AccountRecordSerializer(account)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        account = self.get_object(request, pk)
        serializer = AccountRecordSerializer(account, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    def post(self,request,pk,format=None):
        account = self.get_object(request,pk)
        return_data={'user_count':account.users.all().count(),
                     'max_users' :account.tier.users,
                     'notes_count_active':Notes.objects.filter(account=account).exclude(status__name='archived').count(),
                     'max_notes_active': account.tier.max_notes,
                     'notes_count_archived':Notes.objects.filter(account=account,status__name='archived').count(),
                     'max_retention' : account.tier.retention}

class AccountCheck(APIView):
    """
    Checks to see if the account can have new users or new Notes
    """
    def get_object(self, request, pk):
        try:
            return Account.objects.get(pk=pk)
        except Account.DoesNotExist:
            raise Http404

    def get(self,request,pk,format=None):
        check = accountCheck(request)
        r_data={'Notes':check.canHaveNote(),'Users':check.canHaveUser()}
        return Response(r_data,status=status.HTTP_200_OK)