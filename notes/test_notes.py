__author__ = 'marc'
from django.test import TestCase
from rest_framework.test import APIClient
from .models import *
from django.contrib.auth.models import User
import simplejson
from notes.UserRegistration import *
from django.contrib.auth.models import Group

class NotesTestCase(TestCase):
    def setUp(self):

        self.testuser = User.objects.create_user('test@test.com','test@test.com','testtest')
        self.testuserMod = User.objects.create_user('test@test1.com','test@test1.com','testtest')
        self.testadmin = User.objects.create_superuser('admin@admin.com','admin@admin.com','testtest')
        freeTier = Tier.objects.create(level=0, users=3,max_notes=200, retention=90)
        group=Group(name='Admin Account')
        group.save()
        self.testadmin.groups.add(group)
        self.account=Account.objects.create(name='testAccount',tier=freeTier)
        self.account.users.add(self.testuser)
        self.account.users.add(self.testadmin)
        self.account.save()

        test1=Category.objects.create(name='test1', account=self.account)
        test2 =Category.objects.create(name='test2', account=self.account)
        self.categories=[test1,test2]

        kw1=Keywords.objects.create(keyword='mytest1',account=self.account)
        kw2=Keywords.objects.create(keyword='mytest2',account=self.account)
        try:
            active_status = Status.objects.get(name='active',account=self.account)
        except Status.DoesNotExist:
            active_status = Status(name='active',account=self.account)
            active_status.save()
        try:
            archive_status = Status.objects.get(name='archive',account=self.account)
        except Status.DoesNotExist:
            archive_status = Status(name='archived',account=self.account)
            archive_status.save()
        kw3=Keywords.objects.create(keyword='Thailand',account=self.account)
        self.keywordlist=[kw1,kw2,kw3]
        notes1=Notes.objects.create(user=self.testuser,account=self.account,category=self.categories[0],
                                    body_text='Alpha',status=active_status)
        notes1.keywords.add(kw1,kw2,kw3)
        notes1.save()
        notes2=Notes.objects.create(user=self.testuser,account=self.account,category=self.categories[0],
                                    body_text='Beta', status=active_status)
        notes2.keywords.add(kw1,kw2,kw3)
        notes2.save()

        notes3=Notes.objects.create(user=self.testadmin,account=self.account,category=self.categories[0],
                                    body_text='Gamma',status=archive_status)
        notes3.keywords.add(kw1,kw2,kw3)
        notes3.save()
        newcomment=Comments.objects.create(note=notes1,User=self.testuser,comment_text='my new comment')
        self.notes=[notes1,notes2,notes3]


    def __get_auth_client__(self):
        c=APIClient()
        c.login(username='admin@admin.com', password='testtest')

        return c
    def __get_user_client__(self):
        c=APIClient()
        c.login(username='test@test.com', password='testtest')
        return c

    def test_notes_add(self):
        """
        Add a Note
        :return:
        """
        c = self.__get_auth_client__()
        resp = c.post('/notes/', {'body_text':'test note added','keywords':[x.id for x in self.keywordlist],
                                  'category': self.categories[0].id})
        self.assertContains(resp,'Thailand',status_code=201)

    def test_notes_modify(self):
        """
        Modify a note
        :return:
        """
        c = self.__get_user_client__()
        resp = c.put('/notes/%d/' % self.notes[0].id, {'body_text': 'I have modified this note'})
        self.assertContains(resp,'modified')

        resp = c.post('/notes/%d/' % self.notes[0].id, {'body_text': 'I have modified this note'})
        self.assertNotEqual(resp.status_code,200)

    def test_notes_view_MyNotes(self):
        """
        view only notes I have created
        :return:
        """
        c = self.__get_user_client__()
        resp = c.get('/notes/', {'type' : 'all'})
        print resp
        self.assertContains(resp,'Alpha')
        self.assertContains(resp,'Beta')
        self.assertNotContains(resp,'Gamma')
        self.assertContains((resp,'comment_count'))


    def test_notes_Myaccount(self):
        """
        view notes Created in the account I am in
        :return:
        """
        c = self.__get_user_client__()
        resp = c.get('/notes/', {'type':'all'})
        print resp
        self.assertContains(resp,'Alpha')
        self.assertContains(resp,'Beta')
        self.assertNotContains(resp,'Gamma')
        self.assertContains(resp,'no_likes')
        self.assertContains(resp,'no_dislikes')

        resp = c.get('/notes/', {'page':1,'page_size':8,'type':'none'})
        self.assertContains(resp,'Alpha')
        self.assertContains(resp,'Beta')
        self.assertNotContains(resp,'Gamma')

    def test_notes_getPageCount(self):
        """
        get page count
        :return:
        """
        c = self.__get_user_client__()
        resp = c.get('/notes/', {'type':'all','page_count':'True'})
        self.assertContains(resp,'page_count')

    def test_view_SingleNote(self):
        """
        test to see a single Note
        :return:
        """
        c = self.__get_user_client__()
        resp = c.get('/notes/%d/' % self.notes[0].id)
        self.assertContains(resp,'Alpha')

    def test_RuleTrigger(self):
        """
        Test to see if an Email should be sent for this rule
        :return:
        """
        pass

    def test_notes_poll(self):
        c=self.__get_user_client__()
        resp=c.get('/notes/',{'type':'poll'})
        self.assertEqual(resp.status_code,200)

    def test_notes_like(self):
        c =  self.__get_user_client__()
        payload = {'NoteId':self.notes[0].id,'note_like':False}
        resp = c.post('/likes/', payload)
        self.assertEqual(resp.status_code,200)