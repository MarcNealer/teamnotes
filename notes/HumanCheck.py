__author__ = 'marc'
from random import randint
from django.conf import settings
from hashids import Hashids
import datetime

class humanCheck():
    def __init__(self):
        self.operations=('+','-')
        self.numbers=(('one',1),('two',2),('three',3),('four',4),('five',5),('six',6),('seven',7),('eight',8),('nine',9))
        self.hasher = Hashids(salt=settings.HASH_SALT_KEY, min_length=15)

    def __genQuestion__(self):
        answer = 0
        while answer < 1:
            question = "%d %s %d" % (randint(1,9),self.operations[randint(0,1)],randint(1,9))
            answer = eval(question)
        replaced = False
        while not replaced:
            number=self.numbers[randint(0,8)]
            newQuestion = question.replace(str(number[1]),number[0])
            if number[0] in newQuestion:
                replaced=True
        return (newQuestion,answer)

    def __todaySeconds__(self):
        today=datetime.datetime.now()
        return (today.day*24*60*60)+(today.hour*60*60)+(today.minute*60)+today.second

    def hashAnswer(self,answer):
        return self.hasher.encode(answer)

    def hashNow(self):
        return self.hasher.encode(self.__todaySeconds__())

    def hashNowTest(self):
        return self.hasher.encode(self.__todaySeconds__()-20)

    def __testAnswer__(self,answer,hashanswer):
        return int(answer) == self.hasher.decode(hashanswer)[0]

    def __testDate__(self,hashdate):
        today_seconds=self.__todaySeconds__()
        prev_seconds=self.hasher.decode(hashdate)[0]
        return today_seconds - prev_seconds > 10 and today_seconds - prev_seconds < 2700

    def checkAnswer(self,answer, returnedhash):
        returnedhash=returnedhash.split('/')
        return self.__testAnswer__(answer,returnedhash[0]) and self.__testDate__(returnedhash[1])

    def makeQuestion(self):
        question=self.__genQuestion__()
        return {'question':question[0],'hash':"%s/%s" % (self.hashAnswer(question[1]),self.hashNow())}

    def makeQuestionTest(self):
        question=self.__genQuestion__()
        return {'question':question[0],'hash':"%s/%s" % (self.hashAnswer(question[1]),self.hashNowTest()),'answer':question[1]}
