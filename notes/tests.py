from django.test import TestCase
from rest_framework.test import APIClient
from .models import *
from django.contrib.auth.models import User
import simplejson
from notes.UserRegistration import *
from django.contrib.auth.models import Group
from  notes.HumanCheck import humanCheck

class UserTestCase(TestCase):
    def setUp(self):

        self.testuser = User.objects.create_user('test@test.com','test@test.com','testtest')
        self.testuserMod = User.objects.create_user('test@test1.com','test@test1.com','testtest')
        self.testadmin = User.objects.create_superuser('admin@admin.com','admin@admin.com','testtest')
        freeTier = Tier.objects.create(level=0, users=3,max_notes=200, retention=90)
        group=Group(name='Admin Account')
        group.save()
        self.testadmin.groups.add(group)
        self.account=Account.objects.create(name='testAccount',tier=freeTier)
        self.account.users.add(self.testuser)
        self.account.users.add(self.testadmin)
        self.account.save()

        test1=Category.objects.create(name='test1', account=self.account)
        test2 =Category.objects.create(name='test2', account=self.account)
        self.categories=[test1,test2]

        kw1=Keywords.objects.create(keyword='mytest1',account=self.account)
        kw2=Keywords.objects.create(keyword='mytest2',account=self.account)
        try:
            active_status = Status.objects.get(name='active',account=self.account)
        except Status.DoesNotExist:
            active_status = Status(name='active',account=self.account)
            active_status.save()
        try:
            archive_status = Status.objects.get(name='archived',account=self.account)
        except Status.DoesNotExist:
            archive_status = Status(name='archived',account=self.account)
            archive_status.save()
        self.statusList=[active_status,archive_status]
        kw3=Keywords.objects.create(keyword='Thailand',account=self.account)
        self.keywordlist=[kw1,kw2,kw3]
        notes1=Notes.objects.create(user=self.testuser,account=self.account,category=self.categories[0],
                                    body_text='Alpha',status=active_status)
        notes1.keywords.add(kw1,kw2,kw3)
        notes1.save()
        notes2=Notes.objects.create(user=self.testuser,account=self.account,category=self.categories[0],
                                    body_text='Beta', status=active_status)
        notes2.keywords.add(kw1,kw2,kw3)
        notes2.save()

        notes3=Notes.objects.create(user=self.testadmin,account=self.account,category=self.categories[0],
                                    body_text='Gamma',status=active_status)
        notes3.keywords.add(kw1,kw2,kw3)
        notes3.save()

        self.notes=[notes1,notes2,notes3]

        self.humanQuestion = humanCheck().makeQuestionTest()


    def __get_auth_client__(self):
        c=APIClient()
        c.login(username='admin@admin.com', password='testtest')

        return c
    def __get_user_client__(self):
        c=APIClient()
        c.login(username='test@test.com', password='testtest')
        return c

    def test_create_user(self):
        """
        creates a user
        """
        c=self.__get_auth_client__()
        payload= {'email':'mytest@test.com', 'username':'my_test_user', 'account':'my_first_account',
                  'first_name':'my','last_name':'test','human_answer':self.humanQuestion['answer'],
                  'human_hash':self.humanQuestion['hash']}
        resp=c.post('/create_account/',payload)
        self.assertEqual(resp.status_code,201)
        self.assertContains(resp,'email sent',status_code=201)
        return

    def test_verify_user(self):
        """
        creates a user
        tests the verify url codes
        tests the set password link to ensure password is set
        resends the password link to ensure that the link cannot be used twice
        logs in as the new user and gets the API token
        :return:
        """
        newuser=User.objects.create_user('test1@test1.com','test1@test1.com','testestest')
        newuser.is_active=False
        newuser.save()
        test=NewUserReg()
        url=test.Generate_url(newuser)
        self.assertTrue('verifyuser' in url)
        c=APIClient()
        urldata = '/verifyuser%s' % url.split('verifyuser')[1]
        resp=c.get(urldata)
        self.assertEqual(resp.status_code,200)
        c=self.__get_auth_client__()
        hashdata=urldata.split('/')
        payload = {'user':newuser.id,'password':'testtest','hash1':hashdata[-3],'hash2':hashdata[-2]}
        resp = c.put('/set_password/',payload, format='json')
        self.assertEqual(resp.status_code,200)
        return

    def test_keyword(self):
        testWord = Keywords(keyword='testWord',account=self.account)
        testWord.save()

        # test list
        c=self.__get_auth_client__()
        resp=c.get('/keyword_list/')
        self.assertContains(resp,'testWord')

        #test get

        resp = c.get('/keyword_edit/%d/' % testWord.id)
        self.assertContains(resp,'testWord')
        self.assertContains(resp,'"id":%d' % testWord.id)

        # test add word

        resp = c.post('/keyword_add/', {'keyword': 'test2'})
        self.assertContains(resp,'test2',status_code=201)

        #test update keyword

        resp = c.put('/keyword_edit/%d/' % testWord.id, {'keyword':'updatedWord'})
        self.assertContains(resp,'updatedWord')

        # delete a word

        resp =  c.delete('/keyword_edit/%d/' % testWord.id)
        self.assertEqual(resp.status_code,204)
        resp=c.get('/keyword_list/')
        self.assertNotContains(resp,'testWord')
        self.assertNotContains(resp,'updatedWord')

    def test_account(self):
        """
        test read and write to an account record
        :return:
        """
        c = self.__get_auth_client__()
        resp = c.get('/account/%d/' % self.account.id)
        self.assertContains(resp,'keep_active_notes')
        self.assertContains(resp,'keep_archived_notes')
        self.assertContains(resp,'testAccount')

        resp = c.put('/account/%d/' % self.account.id, {'keep_active_notes': 329667})
        self.assertContains(resp,'329667')

    def test_categories(self):
        """
        test reads and writes to the categories table
        :return:
        """

        c = self.__get_auth_client__()
        resp =  c.get('/category/')
        self.assertContains(resp,self.categories[0].name)
        self.assertContains(resp,self.categories[1].name)

        resp =  c.get('/category_edit/%d/' % self.categories[0].id)
        self.assertContains(resp,self.categories[0].name)

        resp = c.post('/category_add/', {'name':'test3'})
        self.assertContains(resp,'test3',status_code=201)

        resp = c.put('/category_edit/%d/' % self.categories[0].id, {'name':'testAUpdated'})
        self.assertContains(resp,'testAUpdated')
        resp =  c.get('/category/')
        self.assertNotContains(resp,'test1')

        resp = c.delete('/category_edit/%d/' % self.categories[0].id)
        self.assertEqual(resp.status_code,204)
        resp =  c.get('/category/')
        self.assertNotContains(resp,'testAUpdated')

    def test_status(self):
        """
        test reads and writes to the categories table
        :return:
        """

        c = self.__get_auth_client__()
        resp =  c.get('/status/')
        self.assertContains(resp,'active')
        self.assertContains(resp,'archived')

        resp =  c.get('/status_edit/%d/' % self.statusList[0].id)
        self.assertContains(resp,'active')

        resp = c.post('/status_add/', {'name':'broken'})
        self.assertContains(resp,'broken',status_code=201)

        newstatus=Status.objects.create(name='test_status',account=self.account)
        resp = c.put('/status_edit/%d/' % newstatus.id, {'name':'test_status_updated'})
        self.assertContains(resp,'test_status_updated')

        resp = c.delete('/status_edit/%d/' % newstatus.id)
        self.assertEqual(resp.status_code,204)
        resp =  c.get('/status/')
        self.assertNotContains(resp,'test_status_updated')

    def test_rules(self):
        """
        Tests the adding in of rules for email sends
        :return:
        """
        test=Rules(account=self.account,category=self.categories[0])
        test.save()
        test.userlist.add(self.testadmin)
        test.userlist.add(self.testuser)
        test.keywords.add(self.keywordlist[0])
        test.keywords.add(self.keywordlist[2])
        test.save()

        c =  self.__get_auth_client__()
        resp = c.get('/rules/')
        self.assertContains(resp,'"id":%d' % test.id)
        self.assertContains(resp,'keywords')
        self.assertContains(resp,'userlist')


        # test adding a rule
        resp = c.post('/rules/', {'category':self.categories[0].id,
                                  'userlist':[self.testadmin.id, self.testuser.id],
                                  'keywords':[self.keywordlist[0].id,self.keywordlist[1].id]})
        self.assertContains(resp,'saved',status_code=201)

        resp = c.get('/rules/%d/' % test.id)
        self.assertContains(resp,'keywords')
        self.assertContains(resp,'userlist')
        self.assertContains(resp,'"category":1')
        self.assertTrue(self.testadmin.id in simplejson.loads(resp.content)['userlist'])

    def test_myuser(self):
        c =self.__get_user_client__()
        resp = c.get('/myuser/')
        self.assertContains(resp,'test@test.com')
        c =self.__get_auth_client__()
        resp = c.get('/myuser/')
        self.assertContains(resp,'admin@admin.com')
        self.assertEqual(simplejson.loads(resp.content)['groups'][0],"Admin Account")

    def test_user(self):
        c =  self.__get_user_client__()
        resp = c.get('/user/%d/' % self.testuser.id)
        self.assertContains(resp,'test@test.com')

    def test_user_update(self):
        c = self.__get_auth_client__()
        payload={'username':self.testuser.username,'first_name':'updatedName'}
        resp = c.put('/user/%d/' % self.testuser.id,payload)
        self.assertContains(resp,'updatedName')

    def test_account_list(self):
        c= self.__get_user_client__()
        resp = c.get('/account_list/')
        self.assertContains(resp,'test@test.com')

    def test_resent_email(self):
        payload = {'email': self.testuser.email}
        c= APIClient()
        resp = c.post('/resend_email/', payload)
        print resp
        self.assertEqual(resp.status_code,201)

