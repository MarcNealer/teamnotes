from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
import datetime



class Account(models.Model):
    name = models.CharField(max_length=200,db_index=True)
    users = models.ManyToManyField(User)
    keep_active_notes =  models.IntegerField(default=35)
    keep_archived_notes = models.IntegerField(default=100)
    tier=models.ForeignKey('Tier')
    active_until = models.DateField(null=True,blank=True)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.name

class Tier(models.Model):
    level = models.IntegerField()
    users = models.IntegerField()
    max_notes = models.IntegerField()
    retention = models.IntegerField()

class Keywords(models.Model):
    keyword = models.CharField(max_length=100)
    account = models.ForeignKey(Account,db_index=True)

    def __unicode__(self):
        return self.keyword

    class Meta:
        unique_together=('keyword','account')


class Category(models.Model):
    name = models.CharField(max_length=200)
    account =  models.ForeignKey(Account,db_index=True)

    def __unicode__(self):
        return self.name


class Status(models.Model):
    name = models.CharField(max_length=200)
    account = models.ForeignKey('Account',db_index=True)

    def __unicode__(self):
        return self.name

class Notes(models.Model):
    user = models.ForeignKey(User,db_index=True)
    account = models.ForeignKey('Account',db_index=True)
    keywords = models.ManyToManyField('Keywords')
    category = models.ForeignKey('Category')
    body_text = models.TextField()
    status = models.ForeignKey('Status')
    created = models.DateField(default=datetime.date.today())

    def __unicode__(self):
        return "%s (%s)" % (self.body_text[0:50],self.user.username)

    def comment_count(self):
        return Comments.objects.filter(note=self).count()

    def no_likes(self):
        return likes.objects.filter(note=self,like=True).count()
    def no_dislikes(self):
        return likes.objects.filter(note=self,like=False).count()

class Comments(models.Model):
    User = models.ForeignKey(User)
    note = models.ForeignKey('Notes')
    comment_text = models.TextField()

class likes(models.Model):
    User = models.ForeignKey(User)
    note = models.ForeignKey('Notes')
    like = models.BooleanField(default=True)


class AppCosts(models.Model):
    base = models.DecimalField(decimal_places=2,default=3.99,max_digits=5)
    active_notes_price = models.DecimalField(decimal_places=2,default=1.00,max_digits=5)
    active_notes_count = models.IntegerField(default=1000,)
    archived_notes_price = models.DecimalField(decimal_places=2,default=0.50,max_digits=5)
    archived_notes_count = models.IntegerField(default=1000)
    email_price = models.DecimalField(decimal_places=2,default=0.50, max_digits=5)
    email_count = models.IntegerField(default=1000)

class Rules(models.Model):
    account = models.ForeignKey('Account',db_index=True)
    userlist = models.ManyToManyField(User)
    keywords = models.ManyToManyField('Keywords')
    category = models.ForeignKey('Category',null=True,blank=True)

    def __unicode__(self):
        return self.account.name
    def checkKeywords(self,list_of_keywords):
        for words in list_of_keywords:
            if not self.keywords.filter(keyword=words).exists():
                return False
        return True



class WeeklyEmails(models.Model):
    account = models.ForeignKey('Account')
    userlist = models.ManyToManyField(User)
    keywords = models.ManyToManyField('Keywords')
    category = models.ForeignKey('Category',null=True, blank=True)
    weekday = models.IntegerField()



class EmailSendLog(models.Model):
    note = models.ForeignKey('Notes')
    to_user = models.ForeignKey(User)
    by_rule = models.ForeignKey('Rules')
    account = models.ForeignKey('Account',db_index=True)
    date_sent = models.DateField(default=datetime.date.today())

    def __unicode__(self):
        return "%s - Account(%s)" % (self.to_user.username,self.account.name)
