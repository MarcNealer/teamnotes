__author__ = 'marc'
from models import *
import datetime
from itertools import chain
from collections import Counter
import itertools


class NotesExplorer:
    """
    This class is used to create the keyword counts for Notes Explorer
    """
    def __init__(self,request):
        self.user=request.user
        self.data = request.GET


    def __getBaseRecs__(self):
        """
        Tests to see if the user has requested all notes, to just notes for a given
        status. teh default should be active
        :return:
        """
        if self.data['status'] == 'all':
            return Notes.objects.filter(account=Account.objects.get(users=self.user)).order_by("-id")
        else:
            return  Notes.objects.filter(account=Account.objects.get(users=self.user),
                                         status__in=Status.objects.filter(name__iexact=self.data['status'])).order_by("-id")

    def __filterByStartDate__(self,querydata):
        """
        filters on the start date
        :param querydata:
        :return:
        """
        if 'start_date' in self.data:
            return querydata.filter(created__gte= datetime.datetime.strptime(self.data['start_date'],'%Y-%m-%d').date())
        else:
            return querydata
    def __filterByStopDate(self,querydata):
        """
        filters on the stop date
        :param querydata:
        :return:
        """
        if 'stop_date' in self.data:
            return querydata.filter(created__lte= datetime.datetime.strptime(self.data['stop_date'],'%Y-%m-%d').date())
        else:
            return querydata

    def __filterKeywords__(self,noteData):
        for words in self.data.getlist('keywords'):
            noteData=noteData.filter(keywords__id=int(words))
        return noteData

    def __getKeywordCount__(self, noteData):
        return Counter(list(chain.from_iterable([x.keywords.all().values_list('keyword',flat=True) for x in noteData])))
    def __countNoteWithKeyword__(self,keyword):
        # get a count of the numer of notes with a selected keyword
        pass

    def getCount(self):
        noteData= self.__getBaseRecs__()

        noteData =  self.__filterByStopDate(self.__filterByStartDate__(noteData))

        noteData=self.__filterKeywords__(noteData)

        return {'note_count':noteData.count(),'keyword_count': self.__getKeywordCount__(noteData)}

    def getList(self):
        noteData= self.__getBaseRecs__()

        noteData =  self.__filterByStopDate(self.__filterByStartDate__(noteData))

        noteData=self.__filterKeywords__(noteData)

        return noteData
