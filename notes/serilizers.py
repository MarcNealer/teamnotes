__author__ = 'marc'
from rest_framework import serializers
from django.contrib.auth.models import User, Group
from models import *


class AccountSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    username = serializers.CharField(required=True,allow_blank=False,min_length=8, max_length=100)
    first_name = serializers.CharField(required=True, allow_blank=False, max_length=100)
    last_name = serializers.CharField(required=True, allow_blank=False, max_length=100)
    email = serializers.EmailField(required=True, allow_blank=False, max_length=150)
    account = serializers.CharField(required=True,allow_blank=False,min_length=8, max_length=200)

class UserSerialaizer(serializers.Serializer):
    id =  serializers.IntegerField(read_only=True)
    username = serializers.CharField(required=True,allow_blank=False,min_length=8, max_length=100)
    first_name = serializers.CharField(required=True, allow_blank=False, max_length=100)
    last_name = serializers.CharField(required=True, allow_blank=False, max_length=100)
    email = serializers.EmailField(required=True, allow_blank=False, max_length=150)
    account = serializers.CharField(required=True,allow_blank=False,min_length=8, max_length=200)
    is_admin = serializers.BooleanField(required=True)

    def update(self, instance, validated_data):
        instance.username = validated_data.get('username',instance.username)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.email = validated_data.get('email', instance.email)
        if 'is_admin' in validated_data:
            try:
                group=Group.objects.get(name='account_Admin')
            except Group.DoesNotExist:
                group=Group(name='account_Admin')
                group.save()
            instance.groups.add(group)
        return instance



class PasswordSerializer(serializers.Serializer):
    password = serializers.CharField(required=True, allow_blank=False,min_length=8, max_length=100)
    user = serializers.IntegerField(required=True,allow_null=False)

class KeywordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Keywords
        fields =('id','keyword')

class AccountRecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account


class RulesRecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rules

class WeeklyEmailsSeializer(serializers.ModelSerializer):
    class Meta:
        model = WeeklyEmails

class NotesSerializer(serializers.ModelSerializer):
    comment_count =  serializers.IntegerField(read_only=True)
    no_likes = serializers.IntegerField(read_only=True)
    no_dislikes = serializers.IntegerField(read_only=True)
    keywords = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='keyword'
     )
    class Meta:
        model = Notes

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields =('id', 'name')

class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = ('id', 'name')

class NotesExplorerSerializer(serializers.Serializer):
    start_date = serializers.DateField(allow_null=True,required=False)
    stop_date = serializers.DateField(allow_null=True,required=False)
    keywords =  serializers.ListField(child=serializers.CharField(),allow_null=True)
    status = serializers.CharField(min_length=5,max_length=50)

class TierSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tier

class UserModelSerializer(serializers.ModelSerializer):
    groups =serializers.StringRelatedField(many=True)
    class Meta:
        model = User
        fields = ('id', 'first_name','last_name','username','email','last_login','groups','is_active')

class CommentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comments