from django.contrib import admin

# Register your models here.
from models import *

admin.site.register(Account)
admin.site.register(Keywords)
admin.site.register(Category)
admin.site.register(Status)
admin.site.register(Notes)
admin.site.register(AppCosts)
admin.site.register(Rules)
admin.site.register(WeeklyEmails)
admin.site.register(EmailSendLog)
