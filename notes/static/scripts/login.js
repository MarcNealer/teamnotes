var teamNotes = angular.module('teamNotes', ['ngAnimate' , 'ngRoute'])
teamNotes.controller( 'mainController' , function( $scope, $timeout, $http ){
	$scope.formMessages = '';
	
	$scope.submit = function() {
		$scope.pageLoading = true;
		$http.post('/auth/', { username: $scope.lusername , password: $scope.lpassword }).
		success(function(data, status, headers, config) {
			window.location.assign("/");
		}).
		error(function(data, status, headers, config) {
			$scope.formMessages = 'Bad Username or Password. Please try again.';
			$scope.pageLoading = false;
		});
	};
	
	$scope.resetSubmit = function() {
		$scope.pageLoading = true;
		$http.post('/resend_email/', { email: $scope.resetEmail }).
		success(function(data, status, headers, config) {
			$scope.pageLoading = false;
			$scope.reset = false;
			$scope.regSuccess = true;
		}).
		error(function(data, status, headers, config ){
			$scope.formMessages = 'Bad Email address? Please try again.';
			$scope.pageLoading = false;
		});
	};
	
	$scope.regSubmit = function() {
		if( $scope.account == $scope.username ){
			$scope.regFormMessages = 'Account name and username cannot be the same.';
			angular.element('.account').trigger('focus');
			return false;
		}else{ $scope.regFormMessages = ''; }
		
		if( $scope.account == $scope.email ){
			$scope.regFormMessages = 'Account name and Email cannot be the same.';
			angular.element('.account').trigger('focus');
			return false;
		}else{ $scope.regFormMessages = ''; }
		
		if( $scope.username == $scope.email ){
			$scope.regFormMessages = 'Username and Email cannot be the same.';
			angular.element('.username').trigger('focus');
			return false;
		}else{ $scope.regFormMessages = ''; }
		
		if( $scope.regEmail !== $scope.email2 ){ 
			$scope.regFormMessages = 'Email Addresses do not match.';
			angular.element('.regemail').trigger('focus');
			return false;
		}else{ $scope.regFormMessages = ''; }
		
		$scope.pageLoading = true;
		$http.post( '/create_account/',{
					email: $scope.regEmail,
					first_name: $scope.firstName,
					last_name: $scope.lastName, 
					human_answer: $scope.human,
					human_hash: $scope.hashkey,
					username: $scope.username,
					account: $scope.account
				}
		).
		success(function(data, status, headers, config) {
			$scope.regSuccess = true;
			$scope.register = false;
			$scope.pageLoading = false;
		}).
		error(function(data, status, headers, config) {
			$scope.regFormMessages = data;
			if( data == "Human check failed" ){ angular.element('.human').trigger('focus'); }
			if( data == "Account Already Exists" ){ angular.element('.account').trigger('focus'); }
			if( data == "Username Already in use" ){ angular.element('.username').trigger('focus'); }
			$scope.pageLoading = false;
			$("body").animate({scrollTop: 0}, "slow");
		});
	};
	$scope.regPop = function(){
		$scope.register = true;
		$timeout( function(){ angular.element('.account').trigger('focus'); });
		
		$http.get('/humancheck/').
		success(function(data, status, headers, config) {
			$scope.hashkey = data.hash;
			$scope.question = data.question;
		}).
		error(function(data, status, headers, config) { });
	}
	$scope.resetPop = function(){
		$scope.reset = true;
		$timeout( function(){ angular.element('.resetemail').trigger('focus'); });
	}
	$scope.loginPop = function(){
		$scope.reset = false;
		$scope.register = false;
		$timeout( function(){ angular.element('.loginemail').trigger('focus'); });
	}
	$timeout( function(){ angular.element('.loginemail').trigger('focus'); });
});