var teamNotes = angular.module('teamNotes', ['ngAnimate' , 'ngRoute'])
teamNotes.controller( 'mainController' , function( $scope, $http ){
	$scope.formMessages = '';
	$scope.doPassword = function(){
		if( $scope.password !== $scope.password2 ){ $scope.formMessages = 'Passwords do not match.'; angular.element('.password').trigger('focus'); return false; }else{ $scope.formMessages = ''; }
		$scope.pageLoading = true;
		$http.put( "/set_password/" , { hash1: hash1, hash2: hash2, password: $scope.password }, { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
		.success( function(data, status, headers, config){ $scope.success = true; $scope.pageLoading = false; })
		.error( function(data, status, headers, config){ $scope.formMessages = 'There was an error setting your password. Please try again.'; $scope.pageLoading = false; });
	}
});
function getCookie(cname) { var name = cname + "="; var ca = document.cookie.split(';'); for(var i=0; i<ca.length; i++) { var c = ca[i]; while (c.charAt(0)==' ') c = c.substring(1); if (c.indexOf(name) == 0) return c.substring(name.length,c.length); } return ""; }
