var teamNotes = angular.module('teamNotes', ['ngAnimate' , 'ngRoute', 'ui.tinymce', 'ngTouch', 'ngSanitize', 'isteven-multi-select' ])

teamNotes.controller( 'mainController' , function( $scope, $timeout, $swipe, $http ){
// Init vars
	pageSize=8; pagePage=1; $scope.myNotesPage = pagePage; $scope.teamNotesPage = pagePage; $scope.isAdmin = false;
	$scope.currentPollID = 0; $scope.latestPollID = 0; polling = false; $scope.comments =[];
	$scope.weekDays = [{ day:'Monday',id: 0,selected: false },{ day:'Tuesday',id: 1,'selected': false },{ day:'Wednesday',id: 2,'selected': false },{ day:'Thursday',id: 3,'selected': false },{ day:'Friday',id: 4, selected: false },{ day:'Saturday', id: 5, selected: false },{ day:'Sunday', id: 6, selected: false }];
// End Init vars

// Init functions	
	$http.get( '/myuser/')
	.success( function(data, status, headers, config ){ if( data.groups[0] == 'account_Admin' ) $scope.isAdmin = true; $scope.currentUserId = data.id; $scope.currentUserFirst = data.first_name; $scope.currentUserLast = data.last_name; })
	.error( function(data, status, headers, config ){ if( status == '403' )$scope.logout(); });
	
	$http.get( '/tier/')
	.success( function(data, status, headers, config ){ $scope.tier = data; $scope.maxUsers = data.users; $scope.maxNotes = data.max_notes; console.log(data); })
	.error( function(data, status, headers, config ){  });
	
	$http.get( '/account_list/' )
	.success( function(data, status, headers, config ){ $scope.account = data[0]; console.log(data); })
	.error( function(data, status, headers, config ){ });
	
	function pollNotes(){
		$http.get( '/notes/', { params:{ type: 'poll' } }  )
		.success( function(data, status, headers, config ){ $scope.latestPollID = data; })
		.error( function(data, status, headers, config ){ });
		if( polling ){ $timeout.cancel(polling) };
		polling = $timeout( function(){ pollNotes(); }, 120000 )
	}
	
	fetchCats();
	fetchKeys();
	fetchUsers();
// End Init functions
	
// keyword functions
	function fetchKeys(){
		$http.get( '/keyword_list/' )
		.success( function(data, status, headers, config ){
			angular.forEach( data, function( item,i ){ data[i].selected = false; });
			$scope.keywords = data;
			$scope.explorerKeys = angular.copy( data );
			if( ! data.length ){ $scope.notesView = false; $scope.keywordsView = true; $scope.noCatsKeys = true; }
		})
		.error( function(data, status, headers, config ){ });
	}
	
	$scope.addKeyword = function(){
		$scope.savingData = true;
		$scope.popMessages = '';
		
		$http.post( '/keyword_add/', { keyword: $scope.newKeyword }, { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
		.success( function(data, status, headers, config ){
			$scope.keywords.push( { 'keyword': $scope.newKeyword, 'selected': false, 'id': data.id } );
			$scope.newKeyword = '';
			$scope.keywordNewPop = false;
			$scope.savingData = false;
			dataSaved();
		})
		.error( function(data, status, headers, config ){
			if( status == 403 ){ $scope.popMessages = "That keyword already exists."; $scope.savingData = false; }
		});
	}
	
	$scope.deleteKeyword = function(){
		if(! $scope.kwDeleteEnabled ){ return false; }
		haveSelected = false;
		$scope.pageLoading = true;
		$timeout( function(){
			if( !confirm( 'Are you sure you want to DELETE the selected keywords?' ) ){ $scope.pageLoading = false; return false;
			}else{
				angular.forEach( $scope.keywords, function( item,i ){
					if( item.selected ){
						key = item.id;
						$http.delete( '/keyword_edit/' + item.id + '/', { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
						.success(( function( key ){
							return function( data, status, headers, config ){
								angular.forEach( $scope.keywords, function( obj, ind ){
									if( obj.id == key ){ $scope.keywords.splice( ind , 1 ); }
								});
								$scope.pageLoading = false;
								$scope.kwButtons();
							}
						})( key ))
						.error( function(data, status, headers, config ){ });
					}
				});
			}
		}, 300);
	}
	
	$scope.editKeyword = function(){
		if(! $scope.kwEditEnabled ){ return false; }
		$scope.popMessages = '';
		angular.forEach( $scope.keywords, function( item,i ){ if( item.selected ){ $scope.kwEditPopName = item.keyword; $scope.kwEditPopID = item.id; } });
		$scope.keywordEditPop = true;
		$timeout( function(){ angular.element('.edit-key').trigger('focus')} );
	}

	$scope.editKeywordSave = function(){
		$scope.savingData = true;
		$scope.popMessages = '';
		$http.put( '/keyword_edit/' + $scope.kwEditPopID + '/', { keyword: $scope.kwEditPopName }, { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
		.success( function(data, status, headers, config ){
			angular.forEach( $scope.keywords, function( item,i ){ if( item.id == $scope.kwEditPopID ){ item.keyword = $scope.kwEditPopName; } });
			$scope.keywordEditPop = false;
			$scope.savingData = false;
			dataSaved();
		})
		.error( function(data, status, headers, config ){
			if( status == 403 ){ $scope.popMessages = "That keyword already exists."; $scope.savingData = false; }
		});
	}

	$scope.newKeywordPop = function(){ $scope.popMessages = ''; $scope.keywordNewPop  = true; $timeout( function(){ angular.element('.new-key').trigger('focus')} ); }
	$scope.viewKeywords = function(){ closeAllViews();  resetKeywords(); $scope.keywordsView = true; }
	$scope.closeKeywordNewPop = function(){ $scope.keywordNewPop = false; }
	$scope.closeKeywordEditPop = function(){ $scope.keywordEditPop = false; }
	function resetKeywords(){ angular.forEach( $scope.keywords, function( item,i ){ item.selected = false; }); }
	
	$scope.kwButtons = function(){
		$scope.kwDeleteEnabled = false; $scope.kwEditEnabled = false; editCount = 0;
		angular.forEach( $scope.keywords, function( item,i ){ if( item.selected ){ $scope.kwDeleteEnabled = true; editCount ++; } if( editCount == 1 ){ $scope.kwEditEnabled = true; }else{ $scope.kwEditEnabled = false; } });
	}
// End keyword functions	
	

// Category functions
	function fetchCats(){
		$http.get( '/category/' )
		.success( function(data, status, headers, config ){
			$scope.multiCats = [];
			angular.forEach( data, function( item,i ){ item.selected = false; });
			$scope.categories = data;
			if( !data.length ){ $scope.notesView = false; $scope.categoriesView = true; $scope.noCatsKeys = true; }
		})
		.error( function(data, status, headers, config ){ });
	}
	
	$scope.addCategory = function(){
		if( !$scope.newCategory ){ return false; }
		$scope.savingData = true;
		$scope.popMessages = '';
		
		$http.post( '/category_add/', { name: $scope.newCategory }, { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
		.success( function(data, status, headers, config ){
			$scope.categories.push( { 'name': $scope.newCategory, 'selected': false, 'id': data.id } );
			$scope.newCategory = '';
			$scope.categoryNewPop = false;
			$scope.savingData = false;
			dataSaved();
		})
		.error( function(data, status, headers, config ){
			if( status == 403 ){ $scope.popMessages = "That category already exists."; $scope.savingData = false; }
		});
	}
	
	$scope.editCategory = function(){
		if(! $scope.catEditEnabled ){ return false; }
		$scope.popMessages = '';
		angular.forEach( $scope.categories, function( item,i ){ if( item.selected ){ $scope.catEditPopName = item.name; $scope.catEditPopID = item.id; } });
		$scope.categoryEditPop = true;
		$timeout( function(){ angular.element('.edit-cat').trigger('focus')} );
	}
	
	$scope.editCategorySave = function(){
		$scope.savingData = true;
		$scope.popMessages = '';
		
		$http.put( '/category_edit/' + $scope.catEditPopID + '/', { name: $scope.catEditPopName }, { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
		.success( function(data, status, headers, config ){
			angular.forEach( $scope.categories, function( item,i ){ if( item.id == $scope.catEditPopID ){ item.name = $scope.catEditPopName; } });
			$scope.categoryEditPop = false;
			$scope.savingData = false;
			dataSaved();
		})
		.error( function(data, status, headers, config ){
			if( status == 403 ){ $scope.popMessages = "That category already exists."; $scope.savingData = false; }
		});
	}
	
	$scope.deleteCategory = function(){
		if(! $scope.catDeleteEnabled ){ return false; }
		haveSelected = false;
		$scope.pageLoading = true;
		$timeout( function(){
			if( !confirm( 'Are you sure you want to DELETE the selected categories?\n\nIMPORTANT!\nDeleting a category that any notes are using will also delete ALL those notes!!!' ) ){ $scope.pageLoading = false; return false;
			}else{
				angular.forEach( $scope.categories, function( item,i ){
					if( item.selected ){
						key = item.id;
						$http.delete( '/category_edit/' + item.id + '/', { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
						.success(( function( key ){
							return function( data, status, headers, config ){
								angular.forEach( $scope.categories, function( obj, ind ){ if( obj.id == key ){ $scope.categories.splice( ind , 1 ); } });
								$scope.pageLoading = false;
								$scope.catButtons();
							}
						})( key ))
						.error( function(data, status, headers, config ){ });
					}
				});
			}
		},300);
	}	
	
	$scope.categorySelect = function( id ){ angular.forEach( $scope.categories, function( item,i ){ if( item.id == id ){ item.selected = true; }else{ item.selected = false; } }); }
	$scope.newCategoryPop = function(){ $scope.popMessages = ''; $scope.categoryNewPop  = true; $timeout( function(){ angular.element('.new-cat').trigger('focus')} ); }
	$scope.closeCategoryNewPop = function(){ $scope.categoryNewPop  = false; }
	$scope.closeCategoryEditPop = function(){ $scope.categoryEditPop  = false; }
	$scope.viewCategories = function(){ closeAllViews(); resetCategories(); $scope.categoriesView = true; }
	
	function resetCategories(){ angular.forEach( $scope.categories, function( item,i ){ item.selected = false; }); }
	$scope.catButtons = function(){
		$scope.catDeleteEnabled = false; $scope.catEditEnabled = false; editCount = 0;
		angular.forEach( $scope.categories, function( item,i ){ if( item.selected ){ $scope.catDeleteEnabled = true; editCount ++; } if( editCount == 1 ){ $scope.catEditEnabled = true; }else{ $scope.catEditEnabled = false; } });
	}
// End Category functions


// Notes functions
	fetching = 0;
	
	$scope.notesFetchPage = function( page, type ){
		if( type == 'none' ){ $scope.myNotesPage = page; }
		if( type == 'team' ){ $scope.teamNotesPage = page };
		$scope.pageLoading = true;
		
		$http({ url: '/notes/', method: 'GET', params: { page_size: pageSize, page: page, type: type } })
		.success( function(data, status, headers, config ){
			userids = [];
			angular.forEach( data, function( item,i ){
				if( userids.indexOf( item.user ) == -1 ){
					userids.push( item.user )
				}
			});
			angular.forEach( userids, function( item,i ){ getUserData( item , type ); });
			
			if( type == 'none' ){
				$scope.myNotes = data;
				if( data.length && data[0].id > $scope.currentPollID ){ $scope.currentPollID = data[0].id; }
			}
			if( type == 'team' ){
				$scope.teamNotes = data;
				if( data.length && data[0].id > $scope.currentPollID ){ $scope.currentPollID = data[0].id; }
			}
			if( $scope.latestPollID == 0 ){ $scope.latestPollID = $scope.currentPollID; }
			
			$http({ url: '/notes/', method: 'GET', params: { page_size: pageSize, page: $scope.page, page_count: '1', type: type } })
			.success( function(data, status, headers, config ){
				fetching ++;
				if( type == 'none' ){$scope.myNotes.page_count = data[0].page_count;}
				if( type == 'team' ){$scope.teamNotes.page_count = data[0].page_count;}
				
				if( fetching == 2 ){
					$http.get( '/notes_explorer_count/', { params: { status: 'active', keywords: [] } } )
					.success( function(data, status, headers, config ){
						fetching = 0;
						$scope.currentNotesCount = data.max_notes;
						pollNotes();
						$scope.pageLoading = false;
					})
					.error( function(data, status, headers, config ){  });
				}
			})
			.error( function(data, status, headers, config ){ });
		})
		.error( function(data, status, headers, config ){ });
	}
	
	$scope.saveNewNote = function(){
		category = false; keywords = [];
		
		angular.forEach( $scope.categories, function( item,i ){ if( item.selected ){ category = item.id; } });
		if( ! category ){ $scope.newNoteMessages = "Please select a Category"; return false; }
		angular.forEach( $scope.keywords, function( item,i ){ if( item.selected ){ keywords.push( item.id ); } });
		if( ! keywords.length ){ $scope.newNoteMessages = "Please Keywords"; return false; }
		if( ! $scope.tinymceModel ){ $scope.newNoteMessages = "Please enter some note text"; return false; }
		$scope.pageLoading = true;
		
		$http.post( '/notes/', { body_text: $scope.tinymceModel, category: category, keywords: keywords }, { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
		.success( function(data, status, headers, config ){
			$scope.viewNotes();
			$scope.pageLoading = false;
			angular.forEach( $scope.categories, function( item,i ){ item.selected = false;  });
			angular.forEach( $scope.keywords, function( item,i ){ item.selected = false;  });
			$scope.tinymceModel = '';
			$scope.notesFetchPage( $scope.myNotesPage, 'none' );
			$scope.notesFetchPage( $scope.teamNotesPage, 'team' );
			dataSaved();
		})
		.error( function(data, status, headers, config ){ });
	}
	
	$scope.viewNote = function( id, type ){
		closeAllViews();
		angular.forEach( $scope.keywords, function( item,i ){ item.selected = false; });
		
		if( type == 'none' ){ newData = $scope.myNotes; $scope.noteType = 'none'; }
		if( type == 'team' ){ newData = $scope.teamNotes; $scope.noteType = 'team'; }
		
		angular.forEach( newData, function( item,i ){
			if( item.id == id ){
				$scope.fullNoteCreated = item.created;
				$scope.fullNoteBody = item.body_text;
				$scope.editNoteId = item.id;
				angular.forEach( $scope.categories, function( cat,c ){ if( item.category == cat.id ){ cat.selected = true; }else{ cat.selected = false; } });
				angular.forEach( $scope.keywords, function( skey,sk ){ angular.forEach( item.keywords, function( key,k ){ if( key == skey.keyword ){ skey.selected = true; } }); });
				fetchComments();
			}
		});
		$scope.noteView = true;
	}
	
	$scope.saveEditNote = function(){
		if( !$scope.noteEdit )return false;
		category = false; keywords = []; $scope.editNoteMessages = '';
		
		angular.forEach( $scope.categories, function( item,i ){ if( item.selected ){ category = item.id; } });
		if( ! category ){ $scope.editNoteMessages = "Please select a Category"; return false; }
		angular.forEach( $scope.keywords, function( item,i ){ if( item.selected ){ keywords.push( item.id ); } });
		if( ! keywords.length ){ $scope.editNoteMessages = "Please Keywords"; return false; }
		if( ! $scope.fullNoteBody ){ $scope.editNoteMessages = "Please enter some note text"; return false; }
		$scope.pageLoading = true;
		
		$http.put( '/notes/' + $scope.editNoteId + '/', { body_text: $scope.fullNoteBody, category: category, keywords: keywords }, { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
		.success( function(data, status, headers, config ){
			if( $scope.noteType == 'none' ){ newData = $scope.myNotes; }
			if( $scope.noteType == 'team' ){ newData = $scope.teamNotes; }
			
			angular.forEach( newData, function( item,i ){
				if( item.id == $scope.editNoteId ){
					item.category = category;
					item.keywords = [];
					angular.forEach( $scope.keywords, function( key,k ){ angular.forEach( keywords, function( key2,k2 ){ if( key.id == key2 ){ item.keywords.push( key.keyword ); } }); });
					item.body_text = $scope.fullNoteBody;
					$scope.viewNote( item.id, $scope.noteType );
				}
			});
			$scope.pageLoading = false;
			dataSaved();
		})
		.error( function(data, status, headers, config ){ });
	}
	
	function getUserData( id, type ){
		$http.get( '/user/' + id + '/' )
		.success( function(data, status, headers, config ){ if( type == 'none' )newData = $scope.myNotes; if( type == 'team' )newData = $scope.teamNotes; angular.forEach( newData, function( item,i ){ if( item.user == data.id ){ item.name = data.first_name + ' ' + data.last_name; } }); })
		.error( function(data, status, headers, config ){ });
	}
	
	$scope.editNote = function( id ){ if( ! $scope.viewNote ){ return false; }else{ closeAllViews(); $scope.noteEdit = true; } }
	
	$scope.viewNotes = function(){
		closeAllViews();
		$scope.notesFetchPage( 1, 'none' );
		$scope.notesFetchPage( 1, 'team' );
		$scope.notesView = true;
	}
	
	$scope.newNote = function(){ if( $scope.currentNotesCount >= $scope.tier.max_notes ){ $scope.accountLimit(); return false; } closeAllViews(); resetKeywords(); resetCategories(); $scope.newNoteView = true; }
	$scope.cancelNewNote = function(){ closeAllViews(); $scope.notesView = true; }
	$scope.notesFetchPage( $scope.myNotesPage, 'none' );
	$scope.notesFetchPage( $scope.teamNotesPage, 'team' );
	
	$scope.likes = function( state ){
		$http.post( '/likes/', { NoteId: $scope.editNoteId, note_like: state }, { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
		.success( function(data, status, headers, config ){ dataSaved(); })
		.error( function(data, status, headers, config ){ });
		
	}
// End Notes functions

// Comments functions
	function fetchComments(){
		$http.get( '/comments/' + $scope.editNoteId + '/' )
		.success( function(data, status, headers, config ){
			angular.forEach( $scope.myNotes, function(item,i){
				angular.forEach( data, function(dat,d){
					if( dat.user == item.id ){ item.hasComments=true;}else{item.hasComments=false;}
				});
			});
			$scope.comments = data;
		})
		.error( function(data, status, headers, config ){  });
	}
	
	$scope.newComment = function( id ){
		$scope.addComment = true;
		$timeout( function(){ angular.element('.comment-textarea').trigger('focus')},300 );
	}

	$scope.saveComment = function(){
		if( !$scope.newCommentText )return false;
		$scope.savingData = true;
		
		params = {};
		params.NoteId = $scope.editNoteId;
		params.comment_text = $scope.newCommentText;
		
		$http.post( '/comments/', params , { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
		.success( function(data, status, headers, config ){
			$scope.comments.push(data);
			$scope.newCommentID = '';
			$scope.newCommentText = '';
			$scope.savingData = false;
			$scope.addComment = false;
			dataSaved();
		})
		.error( function(data, status, headers, config ){ });
	}
	
	$scope.deleteComment = function( id ){
		$scope.pageLoading = true;
		$timeout(function(){
			if( !confirm( 'Are you sure you want to dlete this comment?' ) ){
				$scope.pageLoading = false; return false;
			}else{
				$http.delete( '/comment_delete/'+ id + '/', { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
				.success( function(data, status, headers, config ){
					fetchComments();
					$scope.pageLoading = false;
					dataSaved();
				})
				.error( function(data, status, headers, config ){ });
			}
		}, 100);
	}
// End comments functions

// Users functions
	function fetchUsers(){
		$scope.userList = [];
		
		$http.get( '/account_list/')
		.success( function(data, status, headers, config ){
			$scope.currentUserCount = 0;
			angular.forEach( data, function( item,i ){
				item.selected = false;
				if( item.is_active ) $scope.userList.push( item );
			});

			$scope.currentUserCount = $scope.userList.length;
			$scope.userActionButtons();
		})
		.error( function(data, status, headers, config ){ });
	}
	
	$scope.userEdit = function(){
		if( ! $scope.userEditEnabled ){ return false; }
		angular.forEach( $scope.userList, function( item,i ){
			if( item.selected ){
				$scope.userEditId = item.id;
				$scope.newUsername = item.username;
				$scope.newUserFirst = item.first_name;
				$scope.newUserLast = item.last_name;
				$scope.newUserEmail = item.email;
			}
			
		});
		$scope.userEditPop = true;
	}
	
	$scope.userDelete = function(){
		$scope.pageLoading = true;
		$timeout( function(){
			if( ! confirm('Are you SURE you want to delete the selected users?') ){ $scope.pageLoading = false; return false;
			}else{
				users = [];
				angular.forEach( $scope.userList, function( item,i ){
					if( item.selected ){
						key = item.id;
						$http.delete( '/user/' + item.id + '/', { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
						.success(( function( key ){
							return function( data, status, headers, config ){
								angular.forEach( $scope.userList, function( item,i ){
									if( item.id == key ){ $scope.userList.splice( i, 1 ); }
								});
								$scope.pageLoading = false;
								dataSaved();
							}
						})( key ))
						.error( function(data, status, headers, config ){ alert('Error! An item WAS NOT deleted.'); $scope.pageLoading = false; });
					}
				});
			}
		});
	}
	
	$scope.saveUser = function(){
		$scope.popFormMessages = '';
		$scope.savingData = true;
		
		data = { username: $scope.newUsername, first_name: $scope.newUserFirst, last_name: $scope.newUserLast, email: $scope.newUserEmail };
		if( $scope.userNewPop ){ url = '/create_user/'; method = 'POST'; }else{ url = '/user/' + $scope.userEditId + '/'; method = 'PUT'; }
		var req = { method: method, url: url, headers: { 'X-CSRFToken': getCookie('csrftoken') }, data: data };
		$http( req )
		.success( function(data, status, headers, config ){
			$scope.pageLoading = false;
			if( $scope.userNewPop ){ $scope.userSuccessMessage = "User Created.<br><br>An email with instructions has been sent to the new user."; }
			if( $scope.userEditPop ){ $scope.userSuccessMessage = "User Changes Saved."; }
			$scope.userNewPop = false;
			$scope.userEditPop = false;
			$scope.userSuccessPop = true;
			$scope.savingData = false;
			angular.forEach( $scope.userList, function( item,i ){
				if( $scope.userEditId == item.id ){
					item.username = $scope.newUsername;
					item.first_name =  $scope.newUserFirst;
					item.last_name =  $scope.newUserLast;
					item.email =  $scope.newUserEmail;
				}
			});
			dataSaved();
		})
		.error( function(data, status, headers, config ){ $scope.popFormMessages = data; $scope.pageLoading = false; });
	}
	
	$scope.userPassword = function(){
		angular.forEach( $scope.userList, function( item,i ){ if( item.selected ){ $scope.userEditId = item.id; } });
		$scope.popFormMessages = ""; $scope.userPasswordPop = true;
		$timeout( function(){ angular.element('.new-pass').trigger('focus')} );
	}
	$scope.savePasswordUser = function(){
		if( $scope.newPassword !== $scope.newPassword2 ){ $scope.popFormMessages = "Passwords do no match"; return false; }
		$scope.popFormMessages = "";
		$scope.savingData = true;
		
		$http.put( '/user/' + $scope.userEditId + '/', { password: $scope.newPassword }, { headers: { 'X-CSRFToken': getCookie('csrftoken') } })
		.success( function(data, status, headers, config ){
			$scope.userSuccessMessage = "User Password Changed.";
			$scope.savingData = false;
			$scope.userPasswordPop = false;
			$scope.userSuccessPop = true;
			$scope.newPassword ='';
			$scope.newPassword2 = '';
			dataSaved();
		})
		.error( function(data, status, headers, config ){ });
	}
	$scope.userActionButtons = function(){ selected = false; angular.forEach( $scope.userList, function( item,i ){ if( item.selected ){ selected ++; } }); if( selected == 1 ){ $scope.userEditEnabled = true; }else{ $scope.userEditEnabled = false; } if( selected ){ $scope.userDeleteEnabled = true; }else{ $scope.userDeleteEnabled = false; } }
	$scope.newUserPop = function(){ $scope.userEditId = ''; $scope.newUsername = ''; $scope.newUserFirst = ''; $scope.newUserLast = ''; $scope.newUserEmail = ''; $scope.userNewPop = true; }	
	$scope.viewUsers = function(){ closeAllViews(); $scope.userList=[]; $timeout( function(){ fetchUsers(); $scope.usersView = true; }, 50 ); }
// End Users functions
	
// Rules functions	
	function fetchRules(){
		$http.get( '/rules/')
		.success( function(data, status, headers, config ){
			$scope.rules = data;
		})
		.error( function(data, status, headers, config ){ });
		
		$http.get( '/weekly/')
		.success( function(data, status, headers, config ){
			$scope.weeklies = data;
		})
		.error( function(data, status, headers, config ){ });
	}
	
	$scope.saveNewRule = function(){
		if( !$scope.newRuleKeys.length || !$scope.newRuleUsers.length ){ return false; }
		params = {};

		if( $scope.newRuleCat.length ){ cats = []; angular.forEach($scope.newRuleCat, function(item,i){ params.category = item.id; })}
		if( $scope.newRuleKeys.length ){ keys=[]; angular.forEach($scope.newRuleKeys, function(item,i){ keys.push(item.id); }); params.keywords = keys; }
		if( $scope.newRuleUsers.length ){ users=[]; angular.forEach($scope.newRuleUsers, function(item,i){ users.push(item.id); }); params.userlist = users; }
		
		$pageLoading = true;
		
		$http.post( '/rules/', params , { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
		.success( function(data, status, headers, config ){
			$scope.newRuleAdd = false;
			angular.forEach($scope.userList, function(item,i){ item.selected=false;});
			angular.forEach($scope.categories, function(item,i){ item.selected=false;});
			angular.forEach($scope.keywords, function(item,i){ item.selected=false;});
			fetchRules();
			$scope.pageLoading = false;
			dataSaved();
		})
		.error( function(data, status, headers, config ){ });
	}

	$scope.saveNewWeekly = function(){
		if( !$scope.newRuleKeys.length || !$scope.newRuleUsers.length || !$scope.newRuleWeekDay ){ return false; }
		params = {};

		if( $scope.newRuleCat.length ){ cats = []; angular.forEach($scope.newRuleCat, function(item,i){ params.category = item.id; })}
		if( $scope.newRuleKeys.length ){ keys=[]; angular.forEach($scope.newRuleKeys, function(item,i){ keys.push(item.id); }); params.keywords = keys; }
		if( $scope.newRuleUsers.length ){ users=[]; angular.forEach($scope.newRuleUsers, function(item,i){ users.push(item.id); }); params.userlist = users; }
		
		params.weekday = $scope.newRuleWeekDay[0].id;
		
		$pageLoading = true;
		
		$http.post( '/weekly/', params , { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
		.success( function(data, status, headers, config ){
			$scope.newRuleAdd = false;
			angular.forEach($scope.userList, function(item,i){ item.selected=false;});
			angular.forEach($scope.categories, function(item,i){ item.selected=false;});
			angular.forEach($scope.keywords, function(item,i){ item.selected=false;});
			fetchRules();
			$scope.pageLoading = false;
			dataSaved();
		})
		.error( function(data, status, headers, config ){ });
	}
	
	$scope.rulesButtons = function(){
		selected = false;
		angular.forEach( $scope.rules, function( item,i ){ if( item.selected ){ selected ++; } } );
		if( selected ){ $scope.ruleDeleteEnabled = true; }else{ $scope.ruleDeleteEnabled = false; }
		if( selected == 1 ){ $scope.ruleEditEnabled = true; }else{ $scope.ruleEditEnabled = false; }
	}
	
	$scope.deleteRules = function(){
		if( !$scope.ruleDeleteEnabled ){ return false; }
		$scope.pageLoading = true;

		$timeout( function(){
			if( !confirm( 'Are you Sure you want to delete the selected rules?' ) ){
				$scope.pageLoading = false;
				return false;
			}else{
				$scope.pageLoading = true;
				selected = []
				var count = 0;
			
				angular.forEach( $scope.rules, function( item,i ){
					if( item.selected ){
						count ++;
						$http.delete( '/rules/' + item.id + '/', { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
						.success( function(data, status, headers, config ){
							count --;
							if( count == 0 ){
								$scope.pageLoading = false;
								fetchRules();
								dataSaved();
							}
						})
						.error( function(data, status, headers, config ){ });
					}
				});
				angular.forEach( $scope.weeklies, function( item,i ){
					if( item.selected ){
						count ++;
						$http.delete( '/weekly/' + item.id + '/', { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
						.success( function(data, status, headers, config ){
							count --;
							if( count == 0 ){
								$scope.pageLoading = false;
								fetchRules();
								dataSaved();
							}
						})
						.error( function(data, status, headers, config ){ });
					}
				});
			}
		});
	}
	$scope.viewRules = function(){ closeAllViews(); fetchRules(); $scope.rulesView = true; }
	
	$scope.editRules = function(){ $scope.rulesEdit = true; }
	
	$scope.newRule = function(){
		resetRulesAdd();	
		$timeout(function(){
			$scope.newRuleAdd = true;
			$scope.newWeeklyAdd = false;
		},200);
	}
	$scope.newWeekly = function(){ 
		resetRulesAdd();
		$timeout(function(){
			$scope.newRuleAdd = false;
			$scope.newWeeklyAdd = true;
		},200);
	}
	
	function resetRulesAdd(){
		angular.forEach ($scope.categories, function(item,i){ item.selected=false; });
		$scope.newRuleCat = '';
		angular.forEach ($scope.keywords, function(item,i){ item.selected=false; });
		$scope.newRuleKeys = '';
		angular.forEach ($scope.userList, function(item,i){ item.selected=false; });
		$scope.newRuleUsers = '';
		angular.forEach ($scope.weekDays, function(item,i){ item.selected=false; });
		$scope.newRuleWeekDay = '';
	}
// End Rules functions

// Account functions	
	$scope.accountLimit = function(){
		console.log('limit');
	}
// End Account functions
	
// Explorer functions
	$scope.filterNotes = function(){
		params = {};
		params.status = $scope.explorerStatus;
		params.page_size = 2000;
		params.page = 1;
		params.keywords = [];
		$scope.noteSelectAll = false;
		
		if( $scope.explorerStart && $scope.explorerEnd ){
			start_date = $scope.explorerStart.toISOString().slice(0, 10);;
			end_date = $scope.explorerEnd.toISOString().slice(0, 10);;
			params.start_date = start_date;
			params.end_date = end_date;
		}
		angular.forEach( $scope.explorerKeys, function( item,i ){ if( item.selected ) params.keywords.push( item.id ); });
				
		$http.get('/notes_explorer_list/',{ params: params } )
		.success( function(data, status, headers, config ){ angular.forEach( data, function( item,i ){ data.selected = false; }); $scope.explorerNotes = data; })
		.error( function(data, status, headers, config ){ });
		
		$http.get('/notes_explorer_count/',{ params: params } )
		.success( function(data, status, headers, config ){
			$scope.explorerKeyStats = [];
			angular.forEach( data.keyword_count, function( item,i ){
				$scope.explorerKeyStats.push({ 'keyword': i, 'keyword_count':item });
			});
			angular.forEach( $scope.explorerKeys, function( item,i ){
				angular.forEach( $scope.explorerKeyStats, function( stat,k ){
					if( item.keyword == stat.keyword ){ stat.selected = item.selected; }
				});
			});
		})
		.error( function(data, status, headers, config ){ });
	}
	
	$scope.keySelect = function ( key ){
		angular.forEach( $scope.explorerKeys, function( item,i ){ if( item.keyword == key ){ item.selected = true } });
		$scope.viewExplorer();
	}
	
	$scope.keyDeSelect = function ( key ){
		angular.forEach( $scope.explorerKeys, function( item,i ){ if( item.keyword == key ){ item.selected = false } });
		$scope.viewExplorer();
	}
	
	$scope.viewExplorer = function(){ closeAllViews(); $scope.filterNotes(); $scope.reportView = false;  $scope.explorerView = true; }
	$scope.viewReport = function(){ closeAllViews(); $scope.reportView = true; $scope.explorerView = true; }
	
	$scope.deleteReportNotes = function(){
		if( !$scope.explorerDeleteEnabled )return false;
		$scope.pageLoading = true;
	
		$timeout( function(){
			if(!confirm('Are you sure you want to DELTE the selected notes?')){
				$scope.pageLoading = false;
				return false;
			}else{ 
				var count = 0;
				angular.forEach( $scope.explorerNotes, function( item, i ){
					if( item.selected ){
						count ++;
						$http.delete( '/notes/'+item.id+'/', { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
						.success( function(data, status, headers, config ){
							count--;
							if( count == 0 ){ $scope.filterNotes(); $scope.pageLoading = false; dataSaved(); }
						})
						.error( function(data, status, headers, config ){ });
					}
				});
			}
		});
	}
	
	$scope.archiveReportNotes = function(){
		if( !$scope.explorerDeleteEnabled )return false;
		$scope.pageLoading = true;
	
		$timeout( function(){
			if(!confirm('Are you sure you want to Archive the selected notes?')){
				$scope.pageLoading = false;
				return false;
			}else{ 
				var count = 0;
				angular.forEach( $scope.explorerNotes, function( item, i ){
					if( item.selected ){
						count ++;
						$http.put( '/notes/'+item.id+'/',{ status: 'archive' }, { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
						.success( function(data, status, headers, config ){
							count--;
							if( count == 0 ){ $scope.filterNotes(); $scope.pageLoading = false; dataSaved(); }
						})
						.error( function(data, status, headers, config ){ });
					}
				});
			}
		});
	}
	
	$scope.reportToggleAll = function(){
		angular.forEach( $scope.explorerNotes, function( item,i ){
			item.selected = $scope.notesToggleAll;
		});
		$scope.updateExplorerButtons();
	}
	$scope.updateExplorerButtons = function(){
		selected = false;
		angular.forEach( $scope.explorerNotes, function( item,i ){
			if( item.selected )selected++;
		});
		if( selected ){ $scope.explorerDeleteEnabled = true; }else{ $scope.explorerDeleteEnabled = false; }
	}

// End Explorer functions
	
//Menu functions
	$scope.clickTab = function(){ if( $scope.hideLeftMenu ){ $scope.hideLeftMenu = false; }else{ if( ! $scope.largeMode )$scope.hideLeftMenu = true; } }
	function clickTab(){ if( $scope.hideLeftMenu ){ $scope.hideLeftMenu = false; }else{ if( ! $scope.largeMode )$scope.hideLeftMenu = true; } }
	$scope.userMenu = function(){  $scope.showUserMenu = !( $scope.showUserMenu ); }
	$scope.menuHoverOut = function(){ $scope.showUserMenu = false; };
	window.onclick = function() { if ($scope.showUserMenu){ $scope.showUserMenu = false; $scope.$apply(); } };
// End Menu functions	
	
	function closeAllViews(){
		$scope.notesView = false;
		$scope.noteView = false;
		$scope.noteEdit = false;
		$scope.newNoteView = false;
		$scope.keywordsView = false;
		$scope.categoriesView = false;
		$scope.usersView = false;
		$scope.rulesView = false;
		$scope.explorerView = false;
		$scope.reportView = false;
		$("body").animate({scrollTop: 0}, "slow");
	}
	
	$scope.tinymceOptions = { height : 300, menubar : false, plugins : 'link textcolor', toolbar1: "| cut copy paste | styleselect | forecolor backcolor | bold italic underline strikethrough subscript superscript removeformat | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | print", save_enablewhendirty: false };
	$scope.tinymceOptions2 = { height : 300, menubar : false, plugins : 'link textcolor', toolbar1: "| cut copy paste | styleselect | forecolor backcolor | bold italic underline strikethrough subscript superscript removeformat | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | print", save_enablewhendirty: false };	
	
	
	// window sizing functions
	function largeScreenDetect(){ if( window.innerWidth >= 1290 ){ $scope.largeMode=true; $scope.hideLeftMenu=false; }else{ $scope.largeMode=false; $scope.hideLeftMenu=true; } }
	if( window.innerWidth > 1290 ){ largeScreenDetect(); }else{ $timeout( clickTab , 1000 ); }
	$(window).resize(function(){ $scope.$apply( function(){ largeScreenDetect(); }); });	
	$scope.rightSwipe = function(){ $scope.hideLeftMenu = false; }
	
	// Paginator functions
	$scope.getNumber = function(num) { return new Array(num); }
	
	
	$scope.logout = function(){ $http.delete( '/auth/', { headers: { 'X-CSRFToken': getCookie('csrftoken') } } ) .success( function(data, status, headers, config ){ window.location.assign( '/' ); }) .error( function(data, status, headers, config ){ }); } 
	
	
	function dataSaved(){
		$scope.dataSavedPop = true;
		$timeout(function(){
			$scope.dataSavedPop = false;
		},5000);
	}
	
	$scope.notesView = true;
});

teamNotes.filter('toTrusted', function ($sce) {
    return function (value) {
        return $sce.trustAsHtml(value);
    };
});

$( document ).ready(function(){
	window.addEventListener("beforeunload", function (e) {
		var confirmationMessage = 'You will lose any unsaved changes.';
		confirmationMessage += '\n\nYou can safely leave the page if you are sure you have no unsaved changes.';

		(e || window.event).returnValue = confirmationMessage; //Gecko + IE
		return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
	});
});

function getCookie(cname) { var name = cname + "="; var ca = document.cookie.split(';'); for(var i=0; i<ca.length; i++) { var c = ca[i]; while (c.charAt(0)==' ') c = c.substring(1); if (c.indexOf(name) == 0) return c.substring(name.length,c.length); } return ""; }
