__author__ = 'marc'
from notes.models import *

class accountCheck():
    def __init__(self,request):
        self.account=Account.objects.get(users=request.user)
        self.tier =self.account.tier
        self.userCount=self.account.users.filter(is_active=True).count()

    def canHaveUser(self):
        return self.tier.users > self.userCount

    def canHaveNote(self):
        noteCount= Notes.objects.filter(account=self.account,
                                        status__name__in=['Active','Archived','active','archived']).count()
        return self.tier.max_notes > noteCount