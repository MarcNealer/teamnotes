__author__ = 'marc'
from django.core.management.base import BaseCommand
from notes.models import *
import datetime


class Command(BaseCommand):

    def handle(self, *args, **options):
        accountsList = Account.objects.filter(active=True)
        for aAccount in accountsList:
            self.__removeNotes__(aAccount)
            self.__archiveNotes__(aAccount)
    def __removeNotes__(self,aAccount):
        keepdays=aAccount.keep_active_notes + aAccount.keep_archived_notes
        removedDate = datetime.datetime.today() -  datetime.timedelta(days=keepdays)
        noteList =  Notes.objects.filter(created_lte=removedDate,account=aAccount).delete()
        return
    def __archiveNotes__(self,aAccount):
        noteList =  Notes.objects.filter(created_lte=datetime.datetime.today()-datetime.timedelta(days=aAccount.keep_active_notes),
                                         account=aAccount).update(status=Status.objects.get(name='archived',account=aAccount))
        return