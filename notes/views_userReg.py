__author__ = 'marc'
from rest_framework.views import APIView
from rest_framework.response import Response
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.permissions import IsAdminUser
from serilizers import AccountSerializer
from serilizers import PasswordSerializer
from serilizers import UserSerialaizer
from serilizers import UserModelSerializer
from models import Account
from django.contrib.auth.models import Group
from UserRegistration import *
from rest_framework.permissions import AllowAny
from django.shortcuts import render
from permissions import IsAppAdmin, IsUserRec
from django.http import Http404
import tierClass
from AccountCheck import *
from notes.HumanCheck import *
from django.contrib.auth import authenticate, login, logout

class UserLogin(APIView):
    permission_classes = (AllowAny,)
    def get(self,request,format=None):
        logout(request)
        return Response('Logged Out',status=status.HTTP_200_OK)
    def post(self,request,format=None):
        if 'username' in request.data and 'password' in request.data:
            user = authenticate(username=request.data['username'],
                                password=request.data['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return Response('logged in',status=status.HTTP_202_ACCEPTED)
                else:
                    return Response('Inactive user',status=status.HTTP_401_UNAUTHORIZED)
            else:
                return Response('bad credentials',status=status.HTTP_401_UNAUTHORIZED)
        else:
            return  Response('bad form',status=status.HTTP_400_BAD_REQUEST)
    def delete(self,request,format=None):
        logout(request)
        return Response('logged out',status=status.HTTP_204_NO_CONTENT)


class UserMyUser(APIView):
    def get(self,request,format=None):
        serializer=UserModelSerializer(request.user)
        return Response(serializer.data,status=status.HTTP_200_OK)


class AccountUsersList(APIView):
    def get(self,request,format=None):
        serializer =UserModelSerializer(Account.objects.get(users=request.user).users.all(),many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)




class UserCreate(APIView):
    # creates a new user in an account. Request has to come from an account admin user
    permission_classes = (IsAppAdmin,)
    def post(self,request, format=None):
        check = accountCheck(request)
        if not check.canHaveUser():
            return Response('Max user reached',status=status.HTTP_405_METHOD_NOT_ALLOWED)
        if User.objects.filter(username=request.data['username']).exists():
            return Response('Username Already in use', status=status.HTTP_400_BAD_REQUEST)
        newUser=User.objects.create_user(request.data['username'],request.data['email'],
                                         last_name=request.data['last_name'],
                                         first_name=request.data['first_name'])
        newUser.is_active = False
        newUser.save()
        account=Account.objects.get(users=request.user)
        account.users.add(newUser)
        email_user=NewUserReg()
        email_user.GenerateUserEmail(newUser,request)
        return Response('created', status=status.HTTP_201_CREATED)

class UserResendEmail(APIView):
    permission_classes = (AllowAny,)
    def post(self,request,format=None):
        if 'email' in request.data:
            userRec=User.objects.filter(email=request.data['email'])
            if userRec.exists():
                email_user=NewUserReg()
                email_user.GenerateUserEmail(userRec[0],request)
                return Response('created', status=status.HTTP_201_CREATED)
            else:
                return Response('email not found',status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response('No email address sent',status=status.HTTP_400_BAD_REQUEST)

class UserUpdate(APIView):
    """
    this class view is used to update a user record. It does not include password resets
    and you cannot change accounts
    """
    permission_classes = (IsUserRec,)
    def get_object(self, request, pk):
        try:
            return User.objects.get(pk=pk,account=Account.objects.filter(users=request.user))
        except User.DoesNotExist:
            raise Http404

    def get(self,request,pk,format=None):
        user = self.get_object(request,pk)
        serializer = UserModelSerializer(user)
        return Response(serializer.data,status=status.HTTP_200_OK)

    def put(self,request, pk,format=None):
        user = self.get_object(request,pk)
        if 'password' in request.data:
            user.set_password(request.data['password'])
            user.save()
            return Response('password reset',status=status.HTTP_200_OK)
        serializer = UserModelSerializer(user,data=request.data,partial=True)
        if serializer.is_valid():
            if 'username' in request.data:
                if not request.data['username'] == user.username:
                    if User.objects.filter(username=request.data['username']).exists():
                        return Response('Username Already in use', status=status.HTTP_400_BAD_REQUEST)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request, pk,format=None):
        user = self.get_object(request,pk)
        user.is_active=False
        user.save()
        return Response('disabled', status=status.HTTP_204_NO_CONTENT)

class AccountCreate(APIView):
    """
    This class is used to create a new base user inside an account
    """
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        try:
            check=humanCheck()
            if not check.checkAnswer(request.data['human_answer'],request.data['human_hash']):
                return Response('Human check failed',status=status.HTTP_417_EXPECTATION_FAILED)
        except:
            return Response('Error in Human check',status=status.HTTP_400_BAD_REQUEST)
        serializer = AccountSerializer(data=request.data)
        if serializer.is_valid():
            if Account.objects.filter(name=request.data['account']).exists():
                return Response('Account Already Exists', status=status.HTTP_400_BAD_REQUEST)
            if User.objects.filter(username=request.data['username']).exists():
                return Response('Username Already in use', status=status.HTTP_400_BAD_REQUEST)
            newAccount=Account(name=request.data['account'],
                               tier=tierClass.getFreeTier())
            newAccount.save()
            newUser=User.objects.create_user(request.data['username'],request.data['email'],
                                             last_name=request.data['last_name'],
                                             first_name=request.data['first_name'])
            newAccount.users.add(newUser)
            newAccount.save()
            try:
                group=Group.objects.get(name='account_Admin')
            except Group.DoesNotExist:
                group=Group(name='account_Admin')
                group.save()
            try:
                active=Status.objects.get(name='active')
            except Status.DoesNotExist:
                active=Status(name='active',account=newAccount)
                active.save()
            try:
                archive=Status.objects.get(name='archived')
            except Status.DoesNotExist:
                active=Status(name='archived',account=newAccount)
                active.save()

            newUser.groups.add(group)
            newUser.is_active = False
            newUser.save()
            email_user=NewUserReg()
            email_user.GenerateAccountEmail(newUser,request)

            return Response('Created', status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class VerifyUser(APIView):
    """
    Get method for user Log
    """
    permission_classes = (AllowAny,)
    def get(self, request, DateHash,UserHash, format=None):
        vuser=NewUserReg()
        resp = vuser.Verify_url(UserHash,DateHash)
        if isinstance(resp,User):
            return render(request,'set_password.html',{'hash1':DateHash,'hash2':UserHash},status=status.HTTP_200_OK)
        else:
            return Response(resp,status=status.HTTP_400_BAD_REQUEST)

class SetPassword(APIView):
    """
    Retrieve, update or delete a code snippet.
    """
    permission_classes =(AllowAny,)
    def put(self, request, format=None):
        vuser=NewUserReg()
        resp = vuser.Verify_url(request.data['hash2'],request.data['hash1'])
        if isinstance(resp,User):
            resp.set_password(request.data['password'])
            resp.is_active=True
            resp.save()
            return Response('Password set',status=status.HTTP_200_OK)
        else:
            return Response('Invalid data',status.HTTP_400_BAD_REQUEST)

