__author__ = 'marc'
__author__ = 'marc'
from django.test import TestCase
from rest_framework.test import APIClient
from .models import *
from django.contrib.auth.models import User
import simplejson
from notes.UserRegistration import *
from django.contrib.auth.models import Group

class NotesExplorerTestCase(TestCase):
    def setUp(self):

        self.testuser = User.objects.create_user('test@test.com','test@test.com','testtest')
        self.testuserMod = User.objects.create_user('test@test1.com','test@test1.com','testtest')
        self.testadmin = User.objects.create_superuser('admin@admin.com','admin@admin.com','testtest')
        freeTier = Tier.objects.create(level=0, users=3,max_notes=200, retention=90)
        group=Group(name='Admin Account')
        group.save()
        self.testadmin.groups.add(group)
        self.account=Account.objects.create(name='testAccount',tier=freeTier)
        self.account.users.add(self.testuser)
        self.account.users.add(self.testadmin)
        self.account.save()

        test1=Category.objects.create(name='test1', account=self.account)
        test2 =Category.objects.create(name='test2', account=self.account)
        self.categories=[test1,test2]

        kw1=Keywords.objects.create(keyword='mytest1',account=self.account)
        kw2=Keywords.objects.create(keyword='mytest2',account=self.account)
        try:
            active_status = Status.objects.get(name='active',account=self.account)
        except Status.DoesNotExist:
            active_status = Status(name='active',account=self.account)
            active_status.save()
        try:
            archive_status = Status.objects.get(name='archive',account=self.account)
        except Status.DoesNotExist:
            archive_status = Status(name='archive',account=self.account)
            archive_status.save()
        kw3=Keywords.objects.create(keyword='Thailand',account=self.account)
        self.keywordlist=[kw1,kw2,kw3]
        notes1=Notes.objects.create(user=self.testuser,account=self.account,category=self.categories[0],
                                    body_text='Alpha',status=active_status)
        notes1.keywords.add(kw1,kw2,kw3)
        notes1.save()
        notes2=Notes.objects.create(user=self.testuser,account=self.account,category=self.categories[0],
                                    body_text='Beta', status=active_status)
        notes2.keywords.add(kw1,kw2,kw3)
        notes2.save()

        notes3=Notes.objects.create(user=self.testadmin,account=self.account,category=self.categories[0],
                                    body_text='Gamma',status=active_status)
        notes3.keywords.add(kw1,kw2,kw3)
        notes3.save()

        notes4=Notes.objects.create(user=self.testadmin,account=self.account,category=self.categories[0],
                                    body_text='Delta',status=active_status,created=datetime.date(2013,5,1))
        notes4.keywords.add(kw1,kw2)
        notes4.save()

        notes5=Notes.objects.create(user=self.testadmin,account=self.account,category=self.categories[0],
                                    body_text='Epselon',status=archive_status,created=datetime.date(2013,5,1))
        notes5.keywords.add(kw1,kw2,kw3)
        notes5.save()

        self.notes=[notes1,notes2,notes3, notes4, notes5]

    def __get_auth_client__(self):
        c=APIClient()
        c.login(username='admin@admin.com', password='testtest')

        return c
    def __get_user_client__(self):
        c=APIClient()
        c.login(username='test@test.com', password='testtest')
        return c

    def test_NotesCount(self):
        """
        gets counts notes against keywords
        :return:
        """
        c = self.__get_user_client__()
        resp = c.get('/notes_explorer_count/', {'status':'active','keywords':[],'page':1,'page_size':8})
        self.assertContains(resp,'"note_count":4')
        self.assertContains(resp,'"mytest1":4')
        self.assertContains(resp,'"mytest2":4')
        self.assertContains(resp,'"Thailand":3')
        resp = c.get('/notes_explorer_count/', {'start_date': '2014-5-1','status':'active','keywords':[]})
        self.assertContains(resp,'"note_count":3')
        self.assertContains(resp,'"mytest1":3')
        self.assertContains(resp,'"mytest2":3')
        self.assertContains(resp,'"Thailand":3')
        resp = c.get('/notes_explorer_count/', {'start_date': '2014-5-1','status':'active','keywords':['Thailand']})
        self.assertContains(resp,'"note_count":3')
        self.assertContains(resp,'"mytest1":3')
        self.assertContains(resp,'"mytest2":3')
        self.assertContains(resp,'"Thailand":3')

    def test_NotesList(self):
        c = self.__get_user_client__()
        resp = c.get('/notes_explorer_list/', {'status':'active','keywords':[],'page':1,'page_size':8})
        self.assertContains(resp,'Alpha')
        self.assertContains(resp,'Thailand')
