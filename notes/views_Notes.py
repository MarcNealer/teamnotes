__author__ = 'marc'
from rest_framework.views import APIView
from rest_framework.response import Response
from models import *
from serilizers import NotesSerializer
from serilizers import NotesExplorerSerializer
from rest_framework import status
from django.http import Http404
from django.core.paginator import Paginator
from EmailManager import InstantEmail
from NotesExplorer import NotesExplorer
from AccountCheck import *

class NotesList(APIView):
    """
    List keywords for a given account.
    """
    def get(self, request, format=None):
        if 'type' in request.GET:
            if 'poll' in request.GET['type']:
                dataobj=Notes.objects.filter(account=Account.objects.get(users=request.user)).exclude(status__name='archived').order_by("-id")
                if dataobj.exists():
                    return Response(dataobj[0].id)
                else:
                    return Response('No notes found')
            if 'page_size' in request.GET:
                page_size = int(request.GET['page_size'])
            else:
                page_size = 25
            if 'all' in request.GET['type']:
                notes = Paginator(Notes.objects.filter(account=Account.objects.get(users=request.user)).exclude(status__name='archived').order_by("-id"),page_size)
            elif 'team' in request.GET['type']:
                notes = Paginator(Notes.objects.filter(account=Account.objects.get(users=request.user)).exclude(status__name='archived').exclude(user=request.user).order_by("-id"),page_size)
            else:
                notes = Paginator(Notes.objects.filter(account=Account.objects.get(users=request.user),user=request.user).exclude(status__name='archived').order_by("-id"),page_size)
            if 'page_count' in request.GET:
                return Response([{'page_count':notes.num_pages}],status=status.HTTP_200_OK)
            if 'page' in request.GET:
                if int(request.GET['page']) <= notes.num_pages:
                    serializer = NotesSerializer(notes.page(int(request.GET['page'])).object_list, many=True)
                else:
                    return Response('Bad Page number', status=status.HTTP_400_BAD_REQUEST)
            else:
                serializer = NotesSerializer(notes.page(1).object_list, many=True)
            return Response(serializer.data)
        else:
            return Response('No Type specified', status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, format=None):

        try:
            check=accountCheck(request)
        except:
            return Response('account check failed',status=status.HTTP_400_BAD_REQUEST)
        if not check.canHaveNote():
            return Response('Note Limit Reached',status=status.HTTP_405_METHOD_NOT_ALLOWED)
        if 'status' in request.data:
            req_status = request.data['status']
        else:
            req_status = 'active'
        try:
            note_status=Status.objects.get(name=req_status,account=Account.objects.get(users=request.user))
        except Status.DoesNotExist:
            note_status=Status.objects.create(name=req_status,account=Account.objects.get(users=request.user))
        try:
            note_cat=Category.objects.get(id=int(request.data['category']),account=Account.objects.get(users=request.user))
        except Category.DoesNotExist:
            return  Response('Error obtaining category record', status=status.HTTP_400_BAD_REQUEST)
        try:
            newNote=Notes(body_text=request.data['body_text'],
                          user=request.user,
                          account=Account.objects.get(users=request.user),
                          status=note_status, category=note_cat)
            newNote.save()
        except:
            return Response('error creating note',status=status.HTTP_400_BAD_REQUEST)

        newNote.keywords.add(*Keywords.objects.filter(id__in=[int(x) for x in request.data['keywords']]))
        newNote.save()
        EmailByRule(newNote)
        serializer=NotesSerializer(newNote)
        return Response(serializer.data, status=status.HTTP_201_CREATED)



class NotesDetail(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """

    def get_object(self, request, pk):
        try:
            return Notes.objects.get(pk=pk,account=Account.objects.filter(users=request.user))
        except Notes.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        notes = self.get_object(request, pk)
        serializer = NotesSerializer(notes)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        try:
            notes = self.get_object(request, pk)
            notes.body_text = request.data.get('body_text',notes.body_text)
            if 'status' in request.data:
                notes.status = Status.objects.get(name=request.data['status'])
            if 'category' in request.data:
                notes.category=Category.objects.get(id=request.data['category'])
            if 'keywords' in request.data:
                notes.keywords.clear()
                for words in request.data['keywords']:
                    notes.keywords.add(Keywords.objects.get(id=words))
            notes.save()
            serializer=NotesSerializer(notes)
            return Response(serializer.data,status=status.HTTP_200_OK)
        except:
            return Response('update failed', status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        notes = self.get_object(request, pk)
        notes.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class NoteExplorerCount(APIView):
    def get(self, request, format=None):
        dataCheck = NotesExplorerSerializer(data=request.GET)
        if dataCheck.is_valid():
            return Response(NotesExplorer(request).getCount(),status=status.HTTP_200_OK)
        else:
            return Response('error', status=status.HTTP_400_BAD_REQUEST)

class NoteExplorerList(APIView):
    def get(self, request, format=None):
        dataCheck = NotesExplorerSerializer(data=request.GET)
        if dataCheck.is_valid():
            if 'page_size' in request.data:
                notes = Paginator(NotesExplorer(request).getList(),int(request.data['page_size']))
            else:
                notes = Paginator(NotesExplorer(request).getList(),15)
            if 'page_count' in request.data:
                return Response([{'page_count':notes.num_pages}],status=status.HTTP_200_OK)
            if 'page' in request.data:
                if int(request.data['page']) <= notes.num_pages:
                    serializer = NotesSerializer(notes.page(int(request.data['page'])).object_list, many=True)
                else:
                    return Response('Bad Page number', status=status.HTTP_403_FORBIDDEN)
            else:
                serializer = NotesSerializer(notes.page(1).object_list, many=True)
            return Response(serializer.data)
        else:
            return Response('error', status=status.HTTP_401_UNAUTHORIZED)