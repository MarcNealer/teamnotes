__author__ = 'marc'
from rest_framework.views import APIView
from rest_framework.response import Response
from permissions import IsUserRec
from permissions import IsAppAdmin
from models import Category
from models import Account
from serilizers import CategorySerializer
from rest_framework import status
from django.http import Http404


class CategoryList(APIView):
    """
    List keywords for a given account.
    """
    def get(self, request, format=None):
        words = Category.objects.filter(account=Account.objects.get(users=request.user))
        serializer = CategorySerializer(words, many=True)
        return Response(serializer.data)


class CategoryAdd(APIView):
    """
    Add a new keyword
    """
    permission_classes = (IsAppAdmin,)
    def post(self, request, format=None):
        if Category.objects.filter(name=request.data['name']).exists():
            return Response(status=status.HTTP_403_FORBIDDEN)
        serializer = CategorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(account=Account.objects.get(users=request.user))
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class CategoryDetail(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    permission_classes = (IsAppAdmin,)
    def get_object(self, request, pk):
        try:
            return Category.objects.get(pk=pk,account=Account.objects.filter(users=request.user))
        except Category.DoesNotExist:
            raise Http404



    def get(self, request, pk, format=None):
        word = self.get_object(request, pk)
        serializer = CategorySerializer(word)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        word = self.get_object(request, pk)
        serializer = CategorySerializer(word, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        word = self.get_object(request, pk)
        word.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)