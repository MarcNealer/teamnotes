__author__ = 'marc'
from notes.models import *

def getFreeTier():
    try:
        free = Tier.objects.get(level=0)
    except Tier.DoesNotExist:
        free = Tier(level=0,users=3,max_notes=200,retention=100)
        free.save()
    return free