__author__ = 'marc'
from rest_framework.views import APIView
from rest_framework.response import Response
from permissions import IsUserRec
from permissions import IsAppAdmin
from models import Status
from models import Account
from serilizers import StatusSerializer
from rest_framework import status
from django.http import Http404

class StatusList(APIView):
    """
    List keywords for a given account.
    """
    def get(self, request, format=None):
        try:
            user_acc=Account.objects.get(users=request.user)
        except Account.DoesNotExist:
            return False
        try:
            active=Status.objects.get(name='active')
        except Status.DoesNotExist:
            active=Status(name='active',account=user_acc)
            active.save()
        try:
            archive=Status.objects.get(name='archive')
        except Status.DoesNotExist:
            active=Status(name='archive',account=user_acc)
            active.save()

        words = Status.objects.filter(account=user_acc)
        serializer = StatusSerializer(words, many=True)
        return Response(serializer.data)


class StatusAdd(APIView):
    """
    Add a new keyword
    """
    permission_classes = (IsAppAdmin,)
    def post(self, request, format=None):
        serializer = StatusSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(account=Account.objects.get(users=request.user))
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class StatusDetail(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    permission_classes = (IsAppAdmin,)
    def get_object(self, request, pk):
        try:
            return Status.objects.get(pk=pk,account=Account.objects.filter(users=request.user))
        except Status.DoesNotExist:
            raise Http404



    def get(self, request, pk, format=None):
        word = self.get_object(request, pk)
        serializer = StatusSerializer(word)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        word = self.get_object(request, pk)
        serializer = StatusSerializer(word, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        word = self.get_object(request, pk)
        word.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)