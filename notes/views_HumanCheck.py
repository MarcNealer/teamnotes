__author__ = 'marc'
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from HumanCheck import humanCheck

class HumanCheck(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        check=humanCheck()
        return Response(check.makeQuestion())