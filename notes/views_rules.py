__author__ = 'marc'
from rest_framework.views import APIView
from rest_framework.response import Response
from permissions import IsAppAdmin
from models import Rules
from models import Account
from models import Keywords
from models import Category
from django.contrib.auth.models import User
from serilizers import RulesRecordSerializer
from rest_framework import status
from django.http import Http404

class RulesList(APIView):
    """
    List keywords for a given Rules.
    """
    permission_classes = (IsAppAdmin,)
    def get(self, request, format=None):
        rule = Rules.objects.filter(account=Account.objects.filter(users=request.user))
        serializer = RulesRecordSerializer(rule, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        newRule=Rules(account=Account.objects.get(users=request.user))
        newRule.save()
        if 'category' in request.data:
            newRule.category=Category.objects.get(id=request.data['category'])
        for word in request.data['keywords']:
            newRule.keywords.add(Keywords.objects.get(id=word))
        for user in request.data['userlist']:
            newRule.userlist.add(User.objects.get(id=user))
        newRule.save()
        return Response('saved', status=status.HTTP_201_CREATED)

class RulesDetail(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    permission_classes = (IsAppAdmin,)
    def get_object(self, request, pk):
        try:
            return Rules.objects.get(pk=pk,account=Account.objects.filter(users=request.user))
        except Rules.DoesNotExist:
            raise Http404



    def get(self, request, pk, format=None):
        rule = self.get_object(request, pk)
        serializer = RulesRecordSerializer(rule)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        rule = self.get_object(request, pk)
        if 'userlist' in request.data:
            rule.userlist.clear()
            try:
                for users in request.data.getlist('users'):
                    rule.userlist.add(User.objects.get(id=users))
            except:
                return Response('Bad user list',status=status.HTTP_400_BAD_REQUEST)
        if 'keywords' in request.data:
            rule.keywords.clear()
            try:
                for words in request.data.getlist('keywords'):
                    rule.keywords.add(Keywords.objects.get(name=words))
            except:
                return Response('bad keyword list',status=status.HTTP_400_BAD_REQUEST)
        if 'category' in request.data:
            try:
                cat = Category.objects.get(name=request.data['category'])
            except:
                return Response('Bad category',status=status.HTTP_400_BAD_REQUEST)
            rule.category=cat
            rule.save()

        return Response('updated', status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        rule = self.get_object(request, pk)
        rule.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)