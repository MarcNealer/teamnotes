__author__ = 'marc'
from models import *

from django.template.loader import render_to_string
from django.core.mail import send_mail
import datetime
from threading import Thread

class InstantEmail():
    def __init__(self,note_object):
        self.note_object = note_object
        self.check()

    def __getAccountRules__(self):
        return Rules.objects.filter(account=Account.objects.get(users=self.note_object.user)).prefetch_related('keywords')

    def __IsEmailReq__(self,rule_object):
        keyword_match = set([x for x in self.note_object.keywords.all()]).issuperset(set([x for x in rule_object.keywords.all()]))
        if rule_object.category:
            return self.note_object.category == rule_object.category and keyword_match
        else:
            return keyword_match

    def __SendEmail__(self,rule_object):
        print 'rendering email'
        html_email_body = render_to_string("note_email.html",
                                           { 'note': self.note_object})
        text_email_body = render_to_string("note_email.txt",
                                           { 'note': self.note_object})
        try:
            print 'sending email'
            send_mail(subject='Instant Email from TeamNotes',
                      message=text_email_body,
                      from_email=settings.DEFAULT_SEND_EMAIL,
                      recipient_list=rule_object.userlist.all().values_list('email',flat=True),
                      html_message=html_email_body)
            return True
        except:
            return False


    def check(self):
        print 'checking emails rules'
        for rule in self.__getAccountRules__():
            print 'checking rule'
            if self.__IsEmailReq__(rule):
                print 'rule match found'
                self.__SendEmail__(rule)


class WeeklyEmail():
    def __getWeeklyRulesForToday__(self):
        return WeeklyEmails.objects.filter(account__active=True,weekday=datetime.datetime.today().weekday()+1)
    def CheckAndSend(self):
        threadList=[]
        for rules in self.__getWeeklyRulesForToday__():
            print 'checking rule'
            thisThread=SendWeeklyEmail(rules)
            threadList.append(thisThread)
            thisThread.start()
            if len(threadList) >= 20:
                for threads in threadList:
                    threads.join()
                threadList =[]

class SendWeeklyEmail(Thread):
    def __init__(self,aRules):
        self.aRules = aRules
    def run(self):
        noteList=self.__getNotesForTheRule__(self.aRules)
        if noteList.exists():
            self.__sendEmail__(noteList,self.aRules)
    def __getNotesForTheRule__(self,rules):
        date_prev=datetime.datetime.today() - datetime.timedelta(days=7)
        notelist=Notes.objects.filter(account=rules.account,created__gte=date_prev)
        for words in rules.keywords.all():
            notelist=notelist.filter(keywords=words)
        if rules.category:
            notelist=notelist.filter(category=rules.category)
        return notelist
    def __sendEmail__(self,noteList,rule_object):
        html_email_body = render_to_string("weeklyEmailReport.html",
                                           { 'note': noteList, 'rule':rule_object})
        text_email_body = render_to_string("weeklyEmailReport.txt",
                                           { 'note': noteList,'rule': rule_object})
        try:
            send_mail(subject='Weekly Email from TeamNotes',
                      message=text_email_body,
                      from_email=settings.DEFAULT_SEND_EMAIL,
                      recipient_list=rule_object.userlist.all().values_list('email',flat=True),
                      html_message=html_email_body)
            return True
        except:
            return False