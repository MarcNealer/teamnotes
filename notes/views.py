from django.shortcuts import render
from django.conf import settings
from django.shortcuts import redirect

def mainLoad(request):
    if request.user.is_authenticated():
        return render(request,'app_main.html')
    else:
        return render(request,'login_page.html')
