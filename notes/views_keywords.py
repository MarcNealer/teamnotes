__author__ = 'marc'
from rest_framework.views import APIView
from rest_framework.response import Response
from permissions import IsUserRec
from permissions import IsAppAdmin
from models import Keywords
from models import Account
from serilizers import KeywordSerializer
from rest_framework import status
from django.http import Http404

class KeywordsList(APIView):
    """
    List keywords for a given account.
    """
    def get(self, request, format=None):
        words = Keywords.objects.filter(account=Account.objects.get(users=request.user))
        serializer = KeywordSerializer(words, many=True)
        return Response(serializer.data)


class KeywordsAdd(APIView):
    """
    Add a new keyword
    """
    permission_classes = (IsAppAdmin,)
    def post(self, request, format=None):
        if Keywords.objects.filter(keyword=request.data['keyword']).exists():
            return Response(status=status.HTTP_403_FORBIDDEN)
        serializer = KeywordSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(account=Account.objects.get(users=request.user))
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class KeywordsDetail(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    permission_classes = (IsAppAdmin,)
    def get_object(self, request, pk):
        try:
            return Keywords.objects.get(pk=pk,account=Account.objects.filter(users=request.user))
        except Keywords.DoesNotExist:
            raise Http404



    def get(self, request, pk, format=None):
        word = self.get_object(request, pk)
        serializer = KeywordSerializer(word)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        word = self.get_object(request, pk)
        serializer = KeywordSerializer(word, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        word = self.get_object(request, pk)
        word.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)