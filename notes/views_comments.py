__author__ = 'marc'
__author__ = 'marc'
from rest_framework.views import APIView
from rest_framework.response import Response
from permissions import IsUserRec
from permissions import IsAppAdmin
from models import *
from serilizers import CommentsSerializer
from rest_framework import status
from django.http import Http404


class CommentAdd(APIView):
    """
    Add a new keyword
    """

    def post(self, request, format=None):
        if 'NoteId' in request.data and 'comment_text' in request.data:
            try:
                commentNote=Notes.objects.get(id=int(request.data['NoteId']))
            except Notes.DoesNotExist:
                return Response('bad note id',status=status.HTTP_400_BAD_REQUEST)
            newComment=Comments.objects.create(User=request.user,
                                               note=commentNote,
                                               comment_text=request.data['comment_text'])
            serializer = CommentsSerializer(newComment)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response('bad form data',status=status.HTTP_400_BAD_REQUEST)

class CommentDetail(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    def get_object(self, request, pk):
        try:
            return Comments.objects.filter(note=Notes.objects.get(id=pk))
        except Comments.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        word = self.get_object(request, pk)
        serializer = CommentsSerializer(word,many=True)
        return Response(serializer.data)

class CommentDelete(APIView):
    permission_classes = (IsAppAdmin,)
    def get_object(self, pk):
        try:
            return Comments.objects.get(id=pk)
        except Comments.DoesNotExist:
            raise Http404
    def delete(self,request,pk):
        rec=self.get_object(pk)
        rec.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class LikesAdd(APIView):
    def post(self,request,format=None):
        if 'NoteId' in request.data and 'note_like' in request.data:
            if not likes.objects.filter(note__id=request.data['NoteId'], User=request.user):
                likes.objects.create(note=Notes.objects.get(id=request.data['NoteId']),User=request.user,like=request.data['note_like'])
            return Response('ok')
        else:
            return Response('bad form',status=status.HTTP_400_BAD_REQUEST)