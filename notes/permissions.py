__author__ = 'marc'
from rest_framework import permissions


class IsUserRec(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """

    def has_object_permission(self, request, view, obj):
        return obj == request.user or request.user.is_staff or request.user.groups.filter(name__icontains='admin').count() > 0

class IsAppAdmin(permissions.BasePermission):
    """
    Check to see if the user is in an admin account group

    """

    def has_object_permission(self, request, view, obj):
        return request.user.groups.filter(name__icontains='admin').count() > 0

class IsAccountAdmin(permissions.BasePermission):
    """
    Check to see if the user is in an admin account group

    """

    def has_object_permission(self, request, view, obj):
        return request.user.groups.filter(name__icontains='account_admin').count() > 0