__author__ = 'marc'
from notes.models import *
import requests
import datetime
from rest_framework.authtoken.models import Token


class WeeklyEmails():
    """
    Usage : for scheduled weekly email sends
    c=WeeklyEmails(request)
    c.sendEmails()
    """
    def __init__(self,request):
        self.server=request.SERVER_NAME
        self.Header={'Authorization':'Token : %s' % Token.objects.get(user=request.user).key}

    def getRules(self):
        return WeeklyEmails.objects.filter(account__active=True,weekday=datetime.datetime.today().weekday())


    def triggerEmailSend(self,record):
        payload={'account_id':record.account.id,
                 'keywords':record.keywords.all().values_list('keyword',flat=True),
                 'emails':record.userlist.all().values_list('email',flat=True)}
        c=requests.post('https://%s/weeklyemailsend/' % self.server,data=payload,headers=self.Header)
        pass
    def sendEmails(self):
        todaysRules=self.getRules()
        for rules in todaysRules:
            self.triggerEmailSend(rules)


class MoveNotes():
    def __init__(self,request):
        self.account=Account.objects.get(user=request.user)
    def getCreateTestDateArchive(self):
        return datetime.datetime.today() -  datetime.timedelta(days=self.account.keep_archived_notes)

    def getCreateTestDateActive(self):
        return datetime.datetime.today() -  datetime.timedelta(days=self.account.keep_active_notes)

    def RemoveOld(self):
        oldNotes =Notes.objects.filter(account=self.account,
                                       status__name_iexact='archived',
                                       created__lte=self.getCreateTestDateArchive()).delete()
    def ArchiveOld(self):
        oldNotes =Notes.objects.filter(account=self.account,
                                       status__name_iexact='active',
                                       created__lte=self.getCreateTestDateActive()).update(Status=Status.objects.get(account=self.account,name='archived'))

class accountManager():
    def removeNotes(self):
        #todo code to remove old archived notes
        pass

