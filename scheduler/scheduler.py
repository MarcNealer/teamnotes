__author__ = 'marc'
from django.db import DatabaseError
from models import scheduled_tasks
import datetime

class taskLock():
    def __init__(self,taskname,cycle):
        self.taskname=taskname
        self.cycle=cycle

    def lock_task(self):
        try:
            self.task_record = scheduled_tasks.objects.select_for_update(nowait=True).filter(name=self.taskname)
            if not self.task_record.exists():
                return False
            else:
                self.task_record=self.task_record[0]
        except DatabaseError:
            return False

    def unlock_task(self):
        self.task_record.last_run=self.cycleValue()
        self.task_record.save()

    def cycleValue(self):
        if self.cycle == 'daily':
            return datetime.datetime.today().weekday()
        if self.cycle == 'weekly':
            return datetime.datetime.today().isocalendar()[1]
        if self.cycle == 'monthly':
            return datetime.datetime.today().month

    def checkIfRun(self):
        return self.task_record.last_run == self.cycleValue()
