from django.db import models

# Create your models here.

class scheduled_tasks(models.Model):
    """
    This model holds records for each controled task in the system.
    You call the record for a lock by the task name.
    You check if it has run by checking the last run field.
    The last run field is just a text field as this could
    be the day of the week, day of the month, or an hour of the day etc
    """
    name = models.CharField(max_length=100)
    last_run = models.CharField(max_length=20)
