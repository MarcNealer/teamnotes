from rest_framework.response import Response
from rest_framework import status
from scheduler import scheduled_tasks
from TaskClasses import *
from rest_framework.views import APIView
# Create your views here.

#def test_task(request,taskname,cycle):
#    mytask=scheduled_tasks(taskname,cycle)
#    mytask.lock_task()
#    if mytask.checkIfRun():
#        mytask.unlock_task()
#        return Response('',status=status.HTTP_409_CONFLICT)
#    # do task here
#    return Response('',status=status.HTTP_200_OK)
# todo create the crontab file for use with ELB

def Weekly_Emails(request,taskname,cycle):
    """
    triggers weekly email sends
    :param request:
    :param taskname:
    :param cycle:
    :return:
    """
    # todo put inn url for starting the weekly email
    mytask=scheduled_tasks(taskname,cycle)
    mytask.lock_task()
    if mytask.checkIfRun():
        mytask.unlock_task()
        return Response('',status=status.HTTP_409_CONFLICT)
    # do task here
    return Response('',status=status.HTTP_200_OK)

class weekly_email_send(APIView):
    def post(self,request,format=None):
        # todo code to send weekly emails to clients
        # todo put in url for weekly email sends
        pass

def remove_old_notes(request,taskname,cycle):
    """
    removes notes in archive status, past the retention period
    :param request:
    :param taskname:
    :param cycle:
    :return:
    """
    # todo put in url for starting the archived notes removal task
    mytask=scheduled_tasks(taskname,cycle)
    mytask.lock_task()
    if mytask.checkIfRun():
        mytask.unlock_task()
        return Response('',status=status.HTTP_409_CONFLICT)
    acc_m=accountManager(request)
    acc_m.removeOldM
    return Response('',status=status.HTTP_200_OK)

def archive_notes(request,taskname,cycle):
    # todo put in the url for starting the deleted old notes code
    mytask=scheduled_tasks(taskname,cycle)
    mytask.lock_task()
    if mytask.checkIfRun():
        mytask.unlock_task()
        return Response('',status=status.HTTP_409_CONFLICT)
    # do task here
    return Response('',status=status.HTTP_200_OK)