from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework.authtoken import views
import notes.views
from notes import views_userReg
from notes import views_keywords
from notes import views_Account
from notes import views_tiers
from notes import views_rules
from notes import views_Notes
from notes import views_categories
from notes import views_Status
from notes import views_WeeklyEmails
from notes import views_HumanCheck
from notes import views_comments
from django.conf.urls.static import static
from django.conf import settings


# starter pattens for admin and DRF
urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


# Account User creation and authentication
urlpatterns += (
    url(r'^main/$',notes.views.mainLoad),
    url(r'^$',notes.views.mainLoad),
    url(r'^auth/$',views_userReg.UserLogin.as_view()),
    url(r'^myuser/$',views_userReg.UserMyUser.as_view()),
    url(r'^create_account/$', views_userReg.AccountCreate.as_view()),
    url(r'^account_list/$',views_userReg.AccountUsersList.as_view()),
    url(r'^verifyuser/(?P<DateHash>[^/]+)/(?P<UserHash>[^/]+)/$',views_userReg.VerifyUser.as_view()),
    url(r'^set_password/$', views_userReg.SetPassword.as_view()),
    url(r'^create_user/$', views_userReg.UserCreate.as_view()),
    url(r'^user/(?P<pk>[0-9]+)/$', views_userReg.UserUpdate.as_view()),
    url(r'^tier/$', views_tiers.TierDetail.as_view()),
    url(r'^humancheck/$',views_HumanCheck.HumanCheck.as_view()),
    url(r'^resend_email/$', views_userReg.UserResendEmail.as_view())
)

# keyword APIS
urlpatterns += (
    url(r'^keyword_list/$',views_keywords.KeywordsList.as_view()),
    url(r'^keyword_add/$',views_keywords.KeywordsAdd.as_view()),
    url(r'^keyword_edit/(?P<pk>[0-9]+)/$',views_keywords.KeywordsDetail.as_view()),
)

# Account review and update API's
urlpatterns += (
    url(r'^account/(?P<pk>[0-9]+)/$', views_Account.AccountDetail.as_view()),
    url(r'^account_check/(?P<pk>[0-9]+)/$',views_Account.AccountCheck.as_view()),
)

# rules list, create and update
urlpatterns += (
    url(r'^rules/$',views_rules.RulesList.as_view()),
    url(r'^rules/(?P<pk>[0-9]+)/$',views_rules.RulesDetail.as_view()),
)

# Weekly Emails list, create and update
urlpatterns += (
    url(r'^weekly/$',views_WeeklyEmails.WeeklyEmailsList.as_view()),
    url(r'^weekly/(?P<pk>[0-9]+)/$',views_WeeklyEmails.WeeklyEmailsDetail.as_view()),
)

# Notes basic urls for listing all from an acocunt, adding and updateing specific records
urlpatterns += (
    url(r'^notes/$',views_Notes.NotesList.as_view()),
    url(r'^notes/(?P<pk>[0-9]+)/$',views_Notes.NotesDetail.as_view()),
    url(r'^notes_explorer_count/$',views_Notes.NoteExplorerCount.as_view()),
    url(r'^notes_explorer_list/$', views_Notes.NoteExplorerList.as_view()),
)

# Category links

urlpatterns += (
    url(r'^category/', views_categories.CategoryList.as_view()),
    url(r'^category_add/$',views_categories.CategoryAdd.as_view()),
    url(r'^category_edit/(?P<pk>[0-9]+)/$',views_categories.CategoryDetail.as_view()),
)

# Status's links

urlpatterns += (
    url(r'^status/$', views_Status.StatusList.as_view()),
    url(r'^status_add/$',views_Status.StatusAdd.as_view()),
    url(r'^status_edit/(?P<pk>[0-9]+)/$',views_Status.StatusDetail.as_view()),
)

# Comments links

urlpatterns += (
    url('^comments/$',views_comments.CommentAdd.as_view()),
    url('^comments/(?P<pk>[0-9]+)/$', views_comments.CommentDetail.as_view()),
    url('^comment_delete/(?P<pk>[0-9]+)/$', views_comments.CommentDelete.as_view()),
    url('^likes/$', views_comments.LikesAdd.as_view())
)

